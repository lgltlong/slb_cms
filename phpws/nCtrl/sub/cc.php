<?php

namespace hpnApp\nCtrl\sub {

use \hpnWse\stNumUtil;
use \hpnWse\stStrUtil;
use \hpnWse\stObjUtil;
use \hpnWse\stAryUtil;
use \hpnWse\stHttpSvc;
use \hpnWse\stSqlUtil;

/// 控制器 - cc
///【注意名字空间是“hpnApp\nCtrl\sub”，前端调用方法为：
/// GET 	http://localhost/phpws/www/api.php?_c=sub/cc&_a=another
///】
class stCtrl_cc extends \hpnWse\stCtrl
{
	// 一个GET接口
	public static function GET_another($a_Help, &$a_Prms)
	{
		if ($a_Help)
		{
			return parent::sdHelp('子控制器接口');
		}

		return array('data' => true);
	}
}

} // namespace hpnApp\nCtrl\sub