<?php

namespace hpnApp\nCtrl {

use \hpnWse\stNumUtil;
use \hpnWse\stStrUtil;
use \hpnWse\stObjUtil;
use \hpnWse\stAryUtil;
use \hpnWse\stDateUtil;
use \hpnWse\stHttpSvc;
use \hpnWse\stSqlUtil;

/// 控制器 - index
class stCtrl_index extends \hpnWse\stCtrl
{
	public function __construct()
	{
		parent::__construct();
		
	}

	/// 默认动作，返回各个接口的帮助信息
	public static function GET_default($a_Help, &$a_Prms)
	{
		return \hpnWse\stHttpSvc::cGetAllCtrlActnHelp();
	}
	
	public static function GET_cgr($a_Help, &$a_Prms)
	{
		if ($a_Help)
		{
			return parent::sdHelp('响应联合GET请求', 
				array('json' => '列出要调用的接口参数，为每个请求起个名字作为键'), 
				array('data' => '对应各个GET请求的响应，键就是请求参数“json”里的键'));
		}

		return stHttpSvc::cRspsCgr(stObjUtil::cFchPpty($a_Prms, 'json'));
	}

	// 一个GET接口
	public static function GET_is_phone_available($a_Help, &$a_Prms)
	{
		if ($a_Help)
		{
			return parent::sdHelp('手机号是否可用？',
				// 接收参数的文档
				array(
					'phone' => 'String, 手机号',
				),
				// 返回数据的文档
				array(
					'data' => 'Boolean，手机号是否可用',
				));
		}

		return array('data' => true);
	}

	// 一个POST接口
	public static function POST_login($a_Help, &$a_Prms)
	{
		if ($a_Help)
		{
			return parent::sdHelp('登录',
				// 接收参数的文档
				array(
					'username' => 'String, 用户名',
					'password' => 'String, 口令',
				),
				// 返回数据的文档
				array(
					'data' => 'Boolean，手机号是否可用'
				));
		}

		// 连接数据库，得到PDO
		$l_Pdo = stHttpSvc::cCnctToDb();

		// 假定发生错误，用这一句报错
		return stHttpSvc::cRspsErr(400, -3, '提交的数据无效！');
	}

	public static function POST_insert($a_Help, &$a_Prms){
		
		$a_Prms['uname'] = 'qimingwang';
		$a_Prms['uid']   = 7;
		$a_Prms['type'] = '起名网';
		$a_Prms['fenlei'] = '初始状态';
		$a_Prms['status'] = 1;

		// 连接数据库，得到PDO
		$l_Pdo = stHttpSvc::cCnctToDb();
		$sql = "INSERT INTO xy_cust SET phone=:ph,title=:hy,dizhi=:dz,addtime=:at,beizhu=:yys,uname=:unm,uid=:uid,type=:tp,fenlei=:fl,status=:st";
		$ex = $l_Pdo->prepare($sql);

		$data = array(
			':ph'  => $a_Prms['ph'],
			':hy'  => $a_Prms['name'],
			':dz'  => $a_Prms['dizhi'],
			':at'  => $a_Prms['time'],
			':yys' => $a_Prms['beizhu'],
			':unm' => 'qimingwang',
			':uid' => 7,
			':tp'  => '起名网',
			':fl'  => '初始状态',
			':st'  => 1
		);

		$res = $ex->execute($data);
		var_dump($res);
	}

	public static function GET_test($a_Help, &$a_Prms){
		echo "file:" . __FILE__ . ', AT LINE:' . __LINE__ . '<BR/>';
		var_dump( $a_Prms );

		echo '---help----';
		var_dump($a_Help);
	}
}

} // namespace hpnApp\nCtrl