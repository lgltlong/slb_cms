<?php

//【会话】
session_start();

// 应用程序目录
$g_AppDiry = dirname(__DIR__) . '/';
require_once($g_AppDiry . 'Wse/hpnWse/(0)Seed.php');
require_once(\hpnWse\fGetWseDiry() . 'hpnWse/SqlUtil.php');

use \hpnWse\stNumUtil;
use \hpnWse\stStrUtil;
use \hpnWse\stObjUtil;
use \hpnWse\stAryUtil;
use \hpnWse\stDateUtil;
use \hpnWse\stHttpSvc;
use \hpnWse\stSqlUtil;

//【在这里配置MySQL连接】
stHttpSvc::$c_PdoCfg = array(
	'c_Dsn' => 'mysql:host=mysql.sql153.eznowdata.com;dbname=sq_lgltlong;',
	'c_Usnm' => 'sq_lgltlong', // 用户名
	'c_Pswd' => 'tanglong0729', // 密码
	'c_Optns' => null,
	'c_Ecdn' => 'UTF8MB4', // 编码，为支持Emoji，用四字节编码
	'c_Salt' => '62661293063646767532', // 盐，用来加密口令
);

//【在这里配置Redis连接】
stHttpSvc::$c_RedisCfg = array(
	'c_Host' => '127.0.0.1',
	'c_Port' => 6379
);

// 初始化
stHttpSvc::cInit(array(
	'c_AppPubDiry' => $g_AppDiry . '/www/',
	'c_AppSrcDiry' => $g_AppDiry,
	'c_RspsStas' => array(
		'c_Time' => true,
		'c_Sql' => true,
		'c_Cch' => true
	)
));


// 接口基本规则：true表示开放给所有人
\hpnWse\stCtrl::$c_GlbRules['GET'] = true;
\hpnWse\stCtrl::$c_GlbRules['POST'] = true;

// 运行
// 接口的入口地址，浏览器直接打开可以看到文档：
// http://localhost/phpws/www/api.php
// 前端Ajax调用示例：
// GET 	http://localhost/phpws/www/api.php?_c=index&_a=is_phone_available
// POST http://localhost/phpws/www/api.php?_c=index&_a=login
stHttpSvc::cRun();
exit;