<?php
/* 单元测试
*
*
*/

namespace hpnWse\nTest {


/// 单元测试
class stUnit
{
	public static $e_TotHtml = '';
	public static $c_ErrCnt = 0;
	public static $e_GrpEc = 0;

	/// 复位
	public static function cRset()
	{
		self::$e_TotHtml = '';
		self::$c_ErrCnt = 0;
		self::$e_GrpEc = 0;
	}

	// /// 获取Html
	// public static function cGetHtml()
	// {
	// 	return self::$e_TotHtml;
	// }

	/// 开始
	/// $a_Tit：String，标题
	public static function cBgn($a_Tit)
	{
		self::$e_TotHtml = '';
		self::$e_TotHtml .= '<div style="margin-bottom: 30px; border: 2px solid {BdrClo};">';
		self::$e_TotHtml .= 
			'<h1 style="background-color: {H1BkgdClo}; padding: 10px; text-align: center; ' . 
			'font-size: 24px; font-weight: bold;">' . 
			$a_Tit . '({ErrCnt})</h1>';
		self::$c_ErrCnt = 0;
		self::$e_GrpEc = 0;
	}

	/// 结束
	public static function cEnd()
	{
		self::$e_TotHtml .= '</div>';

		$l_H1BkgdClo = (self::$c_ErrCnt > 0) ? 'red' : 'lime';
		self::$e_TotHtml = str_replace('{ErrCnt}', self::$c_ErrCnt, self::$e_TotHtml);
		self::$e_TotHtml = str_replace('{H1BkgdClo}', $l_H1BkgdClo, self::$e_TotHtml);
		echo self::$e_TotHtml;
	}

	/// 组
	/// $a_fDo：void f()
	public static function cGrp($a_Tit, $a_fDo)
	{
		self::$e_TotHtml .= '<section style="margin-bottom: 0px; border-bottom: 1px dashed; padding: 10px;">';
		self::$e_TotHtml .= '<h2 style="margin-bottom: 10px; padding: 5px; font-size: 22px; font-weight: bold; ' . 
							'background-color: {H2BkgdClo}; ">' . $a_Tit . '</h2>';
		self::$e_GrpEc = 0;

		self::$e_TotHtml .= '<ol style="list-style-type: decimal-leading-zero; list-style-position: inside;">';
		try
		{
			$a_fDo();
		}
		catch (\Exception $a_Exc)
		{
			++ self::$e_GrpEc;
			++ self::$c_ErrCnt;
			self::$e_TotHtml .= self::eHtml_Li('red',
				$a_Exc->getMessage() . '<br>' . $a_Exc->getFile() . '(' . $a_Exc->getLine() . ')');
		}

		$l_Clo = self::$e_GrpEc ? 'red' : 'lime';
		self::$e_TotHtml = str_replace('{BdrClo}', $l_Clo, self::$e_TotHtml);
		self::$e_TotHtml = str_replace('{H2BkgdClo}', $l_Clo, self::$e_TotHtml);
		self::$e_TotHtml .= '</ol>';
		self::$e_TotHtml .= '</section>';
	}

	private static function eHtml_Li($a_Clo, $a_InrHtml)
	{
		return '<li style="margin-bottom: 10px; color: ' . $a_Clo . ';">' . $a_InrHtml . '</li>';
	}

	private static function eApdHtml($a_Bool, $a_Info)
	{
		$l_Clo = $a_Bool ? 'black' : 'red';
		self::$e_TotHtml .= self::eHtml_Li($l_Clo, $a_Info);
		if (! $a_Bool)
		{
			++ self::$e_GrpEc;
			++ self::$c_ErrCnt;
		}
	}

	/// 信息
	public static function cInfo($a_Info)
	{
		self::eApdHtml(true, $a_Info);
	}

	/// JSON -> HTML
	public static function cJsonToHtml($a_Json, $a_Fmt = null)
	{
		if (\hpnWse\fIsAry($a_Json, true))
		{
			return self::eJsonToHtml_Ary($a_Json, $a_Fmt, 0);
		}
		else
		{
			return self::eJsonToHtml_PureObj($a_Json, $a_Fmt, 0);
		}
	}

	private static function eJsonToHtml_PureObj($a_Json, $a_Fmt, $a_Indt)
	{
		$l_MgnLt = ($a_Indt > 0) ? self::eJsonToHtml_BldMgnLt($a_Fmt) : '';
		$l_Rst = '<div style="' . $l_MgnLt . '">{';
		foreach ($a_Json as $l_Pn => $l_Pv)
		{
			$l_Rst .= '<div style="' . self::eJsonToHtml_BldMgnLt($a_Fmt) . '; ' . self::eJsonToHtml_BldMgnTpBm($a_Fmt) . '">';
			$l_Rst .= '<strong>' . $l_Pn . ': </strong>';

			if (\hpnWse\fIsPureObjOrAry($l_Pv))
			{
				if (\hpnWse\fIsAry($l_Pv, true))
				{
					$l_Rst .= self::eJsonToHtml_Ary($l_Pv, $a_Fmt, $a_Indt + 1);
				}
				else
				{
					$l_Rst .= self::eJsonToHtml_PureObj($l_Pv, $a_Fmt, $a_Indt + 1);
				}
			}
			else
			{
				$l_Rst .= self::eJsonToHtml_OptPrimVal($l_Pv);
				$l_Rst .= ',';
			}
			$l_Rst .= '</div>';
		}

		$l_Rst .= '}' . (($a_Indt > 0) ? ',' : '') . '</div>';
		return $l_Rst;
	}

	private static function eJsonToHtml_Ary($a_Json, $a_Fmt, $a_Indt)
	{
		$l_MgnLt = ($a_Indt > 0) ? self::eJsonToHtml_BldMgnLt($a_Fmt) : '';
		$l_Rst = '<div style="' . $l_MgnLt . '">[';
		for ($i=0; $i<count($a_Json); ++$i)
		{
			$l_Pv = $a_Json[$i];
			if (\hpnWse\fIsPureObjOrAry($l_Pv))
			{
				if (\hpnWse\fIsAry($l_Pv, true))
				{
					$l_Rst .= self::eJsonToHtml_Ary($l_Pv, $a_Fmt, $a_Indt + 1);
				}
				else
				{
					$l_Rst .= self::eJsonToHtml_PureObj($l_Pv, $a_Fmt, $a_Indt + 1);
				}
			}
			else
			{
				$l_Rst .= '<div style="' . self::eJsonToHtml_BldMgnTpBm($a_Fmt) . '">';
				$l_Rst .= self::eJsonToHtml_OptPrimVal($l_Pv);
				$l_Rst .= ',</div>';
			}
		}
		$l_Rst .= ']' . (($a_Indt > 0) ? ',' : '') . '</div>';
		return $l_Rst;
	}

	private static function eJsonToHtml_BldMgnLt($a_Fmt)
	{
		return 'margin-left: 20px';
	}

	private static function eJsonToHtml_BldMgnTpBm($a_Fmt)
	{
		return 'margin-top: 2px; margin-bottom: 2px';
	}

	private static function eJsonToHtml_OptPrimVal($a_Pv)
	{
		if (is_string($a_Pv))
		{ return '<span style="color: orangered;">"' . $a_Pv . '"</span>'; }

		if (is_bool($a_Pv))
		{ return '<span style="color: blue;">' . ($a_Pv ? 'true' : 'false') . '</span>'; }

		return '<span style="color: blue;">' . strval($a_Pv) . '</span>';
	}


	/// 断言
	public static function cAst($a_Exp, $a_Info)
	{
		$l_Bool = boolval($a_Exp);
		self::eApdHtml($l_Bool, $a_Info);
	}

	/// 相等（严格）
	public static function cEq($a_Exp1, $a_Exp2, $a_Info)
	{
		$l_Bool = ($a_Exp1 === $a_Exp2);
		if (! $l_Bool)
		{
			if (is_object($a_Exp1)) { $a_Exp1 = print_r($a_Exp1, true); }
			if (is_object($a_Exp2)) { $a_Exp2 = print_r($a_Exp2, true); }
			$a_Info .=	'<br>$a_Exp1 = “' . strval($a_Exp1) . 
						'”<br>$a_Exp2 = “' . strval($a_Exp2) . '”';
		}
		self::eApdHtml($l_Bool, $a_Info);
	}
}

} // namespace hpnWse\nTest
//////////////////////////////////// OVER ////////////////////////////////////