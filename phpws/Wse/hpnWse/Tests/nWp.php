<?php
use \hpnWse\nTest\stUnit as stUnitTest;

require_once (\hpnWse\fGetWseDiry() . 'hpnWse/nWp/(0)Base.php');
use \hpnWse\nWp;
use \hpnWse\nWp\stBase as stWpBase;

stUnitTest::cGrp('nWp', 
function ()
{

	stUnitTest::cInfo(stWpBase::cHtml_WseCss(
		'(0)Rset.css', 
		'(10)cnWse.css', 
		'(50)OOCSS.css', 
		'Font/font-awesome.css'
	));

	stUnitTest::cInfo(stWpBase::cHtml_WseJs(
		'(0)Seed.js', 
		'(10)nWse.js', 
		'(20)nWse.nMvc.js', 
		'(30)nWse.nGui.js',
		'(31)nWse.nGui.nForm.js'
	));

	stUnitTest::cInfo(stWpBase::cHtml_AppCss(
		'(100)App.css'
	));

	stUnitTest::cInfo(stWpBase::cHtml_AppJs(
		'Cmn.js'
	));

//	stUnitTest::cAst('Mad' == nMvc\fAcsNestPpty($l_Obj, 'name'), '名字是“Mad”。');
//	stUnitTest::cEq(123, nMvc\fAcsNestPpty($l_Obj, 'books[0].isbn'), 'isbn是“123”。');
//	stUnitTest::cInfo(\hpnWse\stObjUtil::cEcdJson($l_BM->cToJson()));
});
//////////////////////////////////// OVER ////////////////////////////////////