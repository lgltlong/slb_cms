<!DOCTYPE html>

<html lang="zh-cmn-Hans">
<head>
	<meta charset="utf-8"/>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style type="text/css">
		html, body, *
		{
			margin: 0;
			border: none;
			padding: 0;
			font-size: 16px;
			font-family: Arial;
		}
	</style>
</head>
<body>
<?php
//【http://localhost/rbhl/Wse/hpnWse/Tests/all.php】

// 冬至引擎种子文件
require_once('../(0)Seed.php');
require_once('../nTest/(0)Unit.php');

use \hpnWse\stBoolUtil;
use \hpnWse\stNumUtil;
use \hpnWse\stStrUtil;
use \hpnWse\stRgxUtil;
use \hpnWse\stAryUtil;
use \hpnWse\stObjUtil;

use \hpnWse\nTest\stUnit as stUnitTest;


stUnitTest::cRset();
stUnitTest::cBgn('全部单元测试');

include_once('BaseFctn.php');
include_once('tJsAry.php');
include_once('nMvc.php');
//include_once('nWp.php');

stUnitTest::cEnd();

?>
</body>
</html>