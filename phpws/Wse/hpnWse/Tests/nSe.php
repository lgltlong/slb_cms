<html>
<body style="word-break: break-all;">
<?php

// Show all errors
error_reporting(E_ALL);

require_once('../(0)Seed.php');
require_once(\hpnWse\fGetWseDiry() . 'hpnWse/nSe/Cws_Hmm.php');
require_once(\hpnWse\fGetWseDiry() . 'hpnWse/nSe/Idxr_Sfx.php');

use \hpnWse\stNumUtil;
use \hpnWse\stStrUtil;
use \hpnWse\stObjUtil;
use \hpnWse\stAryUtil;
use \hpnWse\stDateUtil;
use \hpnWse\nSe\stBase as stSeBase;

// echo stSeBase::cCodeOfCha('我') . '<br>'; // 25105
// echo stSeBase::cChaOfCode(stSeBase::cCodeOfCha('我')) . '<br>';

// $l_Dic1 = new \hpnWse\nSe\tDic();
// $l_Dic1->cAdd('ab');
// echo $l_Dic1->cHas('ab') . '<br>'; // 1
// echo $l_Dic1->cHas('abc') . '<br>'; // 0

// $l_Dic2 = \hpnWse\nSe\tDic::scCopy($l_Dic1);
// $l_Dic2->cAdd('abc');
// echo $l_Dic1->cHas('ab') . '<br>'; // 1
// echo $l_Dic2->cHas('ab') . '<br>'; // 1
// echo $l_Dic1->cHas('abc') . '<br>'; // 0
// echo $l_Dic2->cHas('abc') . '<br>'; // 1

// echo strip_tags(htmlspecialchars('Hello & 世界')); // Hello &amp; 世界
// file_put_contents(__DIR__ . '/abc.txt', htmlspecialchars('Hello & 世界', 0, 'UTF-8'));

echo '<br>';

// $l_Text = <<<TEXT
// ab-你好+(123.456
// 明天会更好！
// TEXT;
// $l_Paras = array();
// stSeBase::cSplTextToParas($l_Paras, $l_Text);
// foreach ($l_Paras as $l_Idx => $l_Para) {
// 	$l_Phrs = array();
// 	stSeBase::cSplStnToPhrs($l_Phrs, $l_Para, count($l_Para));
// 	print_r(stObjUtil::cEcdJson($l_Phrs)); echo '<br>';
// }
// echo '<br>';
// echo '<br>';

$l_Text = '小明，硕士毕业于“中科院”计算所——明天会更好。';
$l_Text = '区块链是一门近几年新兴的技术。';
//$l_Text = '2006年11月12日，我的第一台电脑！';
//$l_Text = '一门近年';
//$l_Text = '邓颖超生前使用过的物品';
// $l_Text = 'Hello World, 使用锤子砸钉子';
//$l_Text = '我十分喜欢这本书';


$l_Text = <<<TEXT
人工智能是过去一年、半年来，所有媒体、创投圈乐此不疲的一个话题。
确实也有很多事实，阿尔法狗碾压了中国的棋手……
很多AI创业公司获得了很抢眼的融资，估值屡创新高。
但作为一个早期投资者来说，我想跟各位创业者吼一声——AI不是风口！
TEXT;

// $l_Text = '我的的确是我的123,456,7890';
// $l_Text = '一个主打按摩保健等服务的在线预约网站及移动应用';
// $l_Text = '123,456,o\'clock,co-author,0Day,C++,c#,五+'; //【各种ASC术语】
// //$l_Text = '小明硕士毕业于“中科院”计算所';
// // $l_Text = '达令';
// $l_Text = '达令™是一家专注于全球好货的电商APP，与海外300多家知名品牌直接签约，商品都是全职买手从世界各地搜罗来的精品好货。2014年中国移动购物用户规模突破3亿，增长速度超过35%，高于PC购物用户25%的增长速度。移动购物的交易规模接近10万亿元，增长率达到270%。与移动购物规模不断增长同样令人瞩目的是，移动购物模式越来越多样化，包括电商平台的手机APP、独立移动电商APP，以及微信购物、手机QQ购物等移动社交电商。';
// // $l_Text = '大学';

// $l_Text = '盈动资本的资金十分雄厚';

$l_Paras = array();
stSeBase::cSplTextToParas($l_Paras, $l_Text);
$l_Stns = array();
for ($i=0; $i<count($l_Paras); ++$i) {
	stSeBase::cSplParaToStns($l_Stns, $l_Paras[$i], count($l_Paras[$i]), null, true);
}
for ($i=0; $i<count($l_Stns); ++$i) {
	$l_Stns[$i] = implode('', $l_Stns[$i]);
}
print_r(stObjUtil::cEcdJson($l_Stns)); 

echo '<br><br>';


$l_Cws = new \hpnWse\nSe\tCws_Hmm();
$l_Cws->cApdDicFromFile('KeyWords', '../../../Se/Keywords.txt');
	//$l_Cws->cSwchDic('KeyWords', 0); // 关闭词典
$l_Cws->c_Dics[0]->cAdd('五+');
$l_Words = array();
$l_Cws->cRun($l_Words, $l_Text);//, true, true);
echo implode(' / ', $l_Words);
// print_r(stObjUtil::cEcdJson($l_Words));

echo '<br>';
echo '<br>';

// $l_Str = 'abc';
// foreach ($l_Str as $key => $value) {
// 	echo $value . ',';
// }

// $l_Dic = new \hpnWse\nSe\tDic();
// $l_Dic->cAdd('你')->cAdd('你好')->cAdd('你好棒')->cAdd('你好棒啊');
// $l_Chas = stStrUtil::cChasFromStr('你');
// echo $l_Dic->cQry($l_Chas) . '<br>';
// $l_Chas = stStrUtil::cChasFromStr('你好');
// echo $l_Dic->cQry($l_Chas) . '<br>';
// $l_Chas = stStrUtil::cChasFromStr('你好棒');
// echo $l_Dic->cQry($l_Chas) . '<br>';
// $l_Chas = stStrUtil::cChasFromStr('你好棒啊');
// echo $l_Dic->cQry($l_Chas) . '<br>';

// $l_Dic->cAddFromFile(\hpnWse\fGetWseDiry() . 'hpnWse/nSe/Dic/StopWords.txt');
// $l_Dic->cAdd('的确');
//print_r(stObjUtil::cEcdJson($l_Dic->c_Trie)); echo '<br>';
// $l_Chas = stStrUtil::cChasFromStr('的确'); echo $l_Dic->cQry($l_Chas) . '<br>';

// $l_Text = '123 我你他 XYZ';
// $l_Text = '清华大学';
// $l_Text = '我的学校？是青岛大学。';

$l_Idxr = new \hpnWse\nSe\tIdxr_Sfx(array(
	'c_RcdTop' => true,
	'c_Cws' => $l_Cws,
	'c_Pdo' => null,
	'c_Tab_Term' => '',
	'c_Tab_Doc' => '',
	'c_Tab_DocCud' => '',
	'c_Tab_TermDocCnt' => '',
	'c_Sp_TermDocCnt' => '',
	'c_Sp_SrchN' => '',
));
$l_Idxr->cAddDftStopDic();
$l_TknMap = array();
$l_TknCnt = 0;
$l_Idxr->cTknzText($l_TknMap, $l_TknCnt, $l_Text, null);
echo '$l_TknCnt = ' . $l_TknCnt . '<br>'; // 50, Hmm分词55
print_r(stObjUtil::cEcdJson($l_TknMap)); echo '<br>';

echo '------------------------------<br>';
// print_r(pack('L', 65)); // A
// $l_Bytes = pack('L', 65); $l_Bytes .= pack('L', 66); $l_Bytes .= pack('L', 67);
// file_put_contents('../../../abc.txt', $l_Bytes); // 12字节
// echo pack("C3",80,72,80);

////////////////////////////////////////////////////////////////////////
// rbhl

function fRbhl($a_Idxr)
{
	echo '<br>';

	require_once('../../../AppBase.php');

	$l_Pdo = \hpnWse\stHttpSvc::cCnctToDb();
	$l_One = \hpnWse\stSqlUtil::cReadRow_Cmpr($l_Pdo, 'rbhl.t_news', 'c_Title, c_Content',
		'c_Id', '=', '251', true);

	$l_Text = strip_tags($l_One['c_Content']);
	echo mb_strlen($l_Text) . '<br>'; // 9865

	$l_TknMap = array();
	$l_TknCnt = 0;
	$a_Idxr->cTknzText($l_TknMap, $l_TknCnt, $l_Text, 90);

	// $l_Keys = array_keys($l_TknMap); print_r($l_Keys); // 6728

	$l_StoPcdrText = '';
	$l_StoPcdrTextLen = 0;
	foreach ($l_TknMap as $l_Term => $l_CntTop) {
		$l_CntTop[0] = strval($l_CntTop[0]);
		$l_CntTop[1] = strval($l_CntTop[1]);

		if ($l_StoPcdrText) { $l_StoPcdrText .= '|'; $l_StoPcdrTextLen += 1; }
		$l_StoPcdrText .= $l_Term; $l_StoPcdrTextLen += mb_strlen($l_Term);
		$l_StoPcdrText .= ':'; $l_StoPcdrTextLen += 1;
		$l_StoPcdrText .= $l_CntTop[0]; $l_StoPcdrTextLen += strlen($l_CntTop[0]);
		$l_StoPcdrText .= ':'; $l_StoPcdrTextLen += 1;
		$l_StoPcdrText .= $l_CntTop[1]; $l_StoPcdrTextLen += strlen($l_CntTop[1]);

		// 65535 一项最多 8 + 1 + 3 + 1 + 3 = 16
		if ($l_StoPcdrTextLen >= 65535 - 16)
		{
			echo '-----------------------------------------------<br>';

			$l_StoPcdrText = '';
			$l_StoPcdrTextLen = 0;
		}
	}

//	echo mb_strlen($l_StoPcdrText) . ' = ' . $l_StoPcdrTextLen . '<br>'; // 8=69779, 4=60107

	print_r(stObjUtil::cEcdJson($l_TknMap)); echo '<br>';
}
// fRbhl($l_Idxr);


////////////////////////////////////////////////////////////////////////
?>
</body>
</html>