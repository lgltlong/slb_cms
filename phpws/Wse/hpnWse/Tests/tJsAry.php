<?php
use \hpnWse\stBoolUtil;
use \hpnWse\stNumUtil;
use \hpnWse\stStrUtil;
use \hpnWse\stRgxUtil;
use \hpnWse\stAryUtil;
use \hpnWse\stObjUtil;

use \hpnWse\nTest\stUnit as stUnitTest;

use \hpnWse\tJsAry;

stUnitTest::cGrp('tJsAry', 
function ()
{
	$l_A = new tJsAry([]);
	for ($i=1; $i<6; ++$i)
	{
		$l_A->push($i);
	}
	stUnitTest::cEq('1,2,3,4,5', $l_A->join(), 
		'push(1...5)后，$l_A->join() == "1,2,3,4,5"');

	$l_A->unshift(0);
	stUnitTest::cEq('0,1,2,3,4,5', $l_A->join(), 
		'unshift(0)后，$l_A->join() == "0,1,2,3,4,5"');

	$l_Out = $l_A->pop();
	stUnitTest::cEq(5, $l_Out, 
		'pop()后，$l_Out == 5，且……');
	stUnitTest::cEq('0,1,2,3,4', $l_A->join(), 
		'$l_A->join() == "0,1,2,3,4"');

	$l_Out = $l_A->shift();
	stUnitTest::cEq(0, $l_Out, 
		'shift()后，$l_Out == 0，且……');
	stUnitTest::cEq('1,2,3,4', $l_A->join(), 
		'$l_A->join() == "1,2,3,4"');

	$l_Out = $l_A->splice(1, 2, "A", "B", "C");
	stUnitTest::cEq('2,3', $l_Out->join(), 
		'splice(1, 2, "A", "B", "C")后，$l_Out->join() == "2,3"，且……');
	stUnitTest::cEq('1,A,B,C,4', $l_A->join(), 
		'$l_A->join() == "1,A,B,C,4"');	

	$l_Out = $l_A->slice(2, 9);
	stUnitTest::cEq('B,C,4', $l_Out->join(), 
		'slice(2, 9)后，$l_Out->join() == "B,C,4"');

	$l_Out = $l_A->slice(-3, 2);
	stUnitTest::cEq('1,A', $l_Out->join(), 
		'slice(-3, 2)后，$l_Out->join() == "1,A"');

	$l_Out = $l_A->slice(1, 4);
	stUnitTest::cEq('A,B,C', $l_Out->join(), 
		'slice(1, 4)后，$l_Out->join() == "A,B,C"');

	$l_A[5] = 5;
	stUnitTest::cEq(6, $l_A->length, 
		'$l_A[5] = 5后，$l_A->length == 6，且……');
	stUnitTest::cEq('1,A,B,C,4,5', $l_A->join(), 
		'$l_A->join() == "1,A,B,C,4,5"');

	$l_A->length = 3;
	stUnitTest::cEq(3, $l_A->length, 
		'$l_A->length = 3后，$l_A->length == 3，且……');
	stUnitTest::cEq('1,A,B', $l_A->join(), 
		'$l_A->join() == "1,A,B"');

	$l_A->push("C", "A", "D");
	stUnitTest::cEq('1,A,B,C,A,D', $l_A->join(), 
		'push("C", "A", "D")后，$l_A->join() == "1,A,B,C,A,D"');

	$l_Idx = $l_A->indexOf("A");
	stUnitTest::cEq(1, $l_Idx, 
		'indexOf("A")后，$l_Idx == 1');

	$l_Idx = $l_A->lastIndexOf("A");
	stUnitTest::cEq(4, $l_Idx, 
		'lastIndexOf("A")后，$l_Idx == 4');

	//【没问题！】
	//\hpnWse\stAryUtil::cFor($l_A, function ($a_A, $a_I, $a_E) { var_dump($a_E); });

});
//////////////////////////////////// OVER ////////////////////////////////////