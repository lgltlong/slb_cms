<?php
use \hpnWse\stBoolUtil;
use \hpnWse\stNumUtil;
use \hpnWse\stStrUtil;
use \hpnWse\stRgxUtil;
use \hpnWse\stAryUtil;
use \hpnWse\stObjUtil;

use \hpnWse\nTest\stUnit as stUnitTest;

use \hpnWse\tJsAry;

// preg_replace("/(\s|\&nbsp\;|　|\xc2\xa0)/","",$content); 全角空白
// mb_internal_encoding( 'UTF-8' );

require_once(\hpnWse\fGetDploDiry() . 'PhpVendor/PHPMailer/class.phpmailer.php');
require_once(\hpnWse\fGetDploDiry() . 'PhpVendor/PHPMailer/class.smtp.php');
require_once(\hpnWse\fGetWseDiry() . 'hpnWse/SqlUtil.php');

stUnitTest::cGrp('基础功能', 
function ()
{
/*
$l_Sql = <<<SQL
abc
SQL;
	$l_Sql .= 'def';
	stUnitTest::cInfo('“<<<”换行测试：<pre>' . $l_Sql . '</pre>');

$l_Sql = <<<SQL
abc

SQL;
	$l_Sql .= 'def';
	stUnitTest::cInfo('“<<<”换行测试：<pre>' . $l_Sql . '</pre>');

// 有个空格
$l_Sql = <<<SQL
abc 
SQL;
	$l_Sql .= 'def';
	stUnitTest::cInfo('“<<<”换行测试：<pre>' . $l_Sql . '</pre>');
//*/

	stUnitTest::cInfo('上传文件名？' . \hpnWse\stSqlUtil::cMayBeUpldFlnm('bc1d99d3767cb7c4d82a897f66a4a016.jpg'));

	stUnitTest::cInfo('Web根URL：' . \hpnWse\fGetWebRootUrl());
	stUnitTest::cInfo('部署目录：' . \hpnWse\fGetDploDiry());
	stUnitTest::cInfo('WSE目录：' . \hpnWse\fGetWseDiry());

	function fStrUtil()
	{
		stUnitTest::cInfo('测试stStrUtil：');

$l_Html = <<<HTML
<h1 onclick="console.log(this);">这是标题</h1>
<div>
	<a href="http://baidu.com">你好，世界！</a>
	<span>The End</span>
</div>
<script>
	alert('script');
</script>
<script type="text/javascript">
	alert('script');
</script>
<iframe src="javascript:alert('<iframe>')"></iframe>
<div><a href="javascript:alert('<a>')"></a></div>
<script type="text/vertex-shader">float4 rgb;</script>
HTML;

$l_Html2 = <<<HTML
<h1>这是标题</h1><div>
	<a href="http://baidu.com">你好，世界！</a>
	<span>The End</span>
</div><iframe></iframe><div><a></a></div><script type="text/vertex-shader">float4 rgb;</script>
HTML;


	//	stUnitTest::cInfo(\hpnWse\stStrUtil::cEcdHtmlScha(\hpnWse\stStrUtil::cRmvJavaScript($l_Html)));
		stUnitTest::cEq(
			\hpnWse\stStrUtil::cFind(
				\hpnWse\stStrUtil::cEcdHtmlScha(\hpnWse\stStrUtil::cRmvJavaScript($l_Html)),
				'alert'), -1, 'cEcdHtmlScha + cRmvJavaScript');

		// stUnitTest::cEq(
		// 	\hpnWse\stStrUtil::cEcdHtmlScha(\hpnWse\stStrUtil::cRmvJavaScript($l_Html)),
		// 	\hpnWse\stStrUtil::cEcdHtmlScha($l_Html2), //【最后差个换行符，可能是win“\r\n”】
		// 	'cEcdHtmlScha + cRmvJavaScript');

		stUnitTest::cEq(\hpnWse\stStrUtil::cRplcPlchd('字符个数有效范围是：{{c_Min}} - {{c_Max}} 个！',
						['c_Min'=>6, 'c_Max'=>20]),
						'字符个数有效范围是：6 - 20 个！', 'cRplcPlchd(, 数组)');
		$l_Obj = new stdClass();
		$l_Obj->c_Min = 6;
		$l_Obj->c_Max = 20;
		stUnitTest::cEq(\hpnWse\stStrUtil::cRplcPlchd('字符个数有效范围是：{{c_Min}} - {{c_Max}} 个！',
						$l_Obj),
						'字符个数有效范围是：6 - 20 个！', 'cRplcPlchd(, 对象)');

		stUnitTest::cInfo(date('Y-m', time()));

		stUnitTest::cInfo(\hpnWse\stStrUtil::cSub('奥克兰市是……', 3));
	}
	fStrUtil();

	function fSqlUtil()
	{
		stUnitTest::cInfo('测试stSqlUtil');
		stUnitTest::cEq(\hpnWse\stSqlUtil::cRmvDngrCha('a\'b"c/d\\e'), 'abcde', '移除危险字符：“a\'b"c/d\\e”->“abcde”');

		$i_Rgx_TabAs = '/(int\\s+|float\\s+)?\\w+(?:\\s+as\\s+(\\w+))?/i';

		$l_IsMch = preg_match($i_Rgx_TabAs, "c_ShortName", $l_Mch);
		stUnitTest::cEq(isset($l_Mch[1]), false, '捕获[1] = null');
		stUnitTest::cEq(isset($l_Mch[2]), false, '捕获[2] = null');

		$l_IsMch = preg_match($i_Rgx_TabAs, "int c_ShortName", $l_Mch);
		stUnitTest::cEq($l_Mch[1], 'int ', '捕获[1] = “int ”');
		stUnitTest::cEq(isset($l_Mch[2]), false, '捕获[2] = null');

		$l_IsMch = preg_match($i_Rgx_TabAs, "c_ShortName AS c_CompName", $l_Mch);
		stUnitTest::cEq($l_Mch[1], '', '捕获[1] = ""');
		stUnitTest::cEq($l_Mch[2], 'c_CompName', '捕获[2] = “c_CompName”');

		$l_IsMch = preg_match($i_Rgx_TabAs, "int c_ShortName AS c_CompName", $l_Mch);
		stUnitTest::cEq($l_Mch[1], 'int ', '捕获[1] = “int ”');
		stUnitTest::cEq($l_Mch[2], 'c_CompName', '捕获[2] = “c_CompName”');
	}
	fSqlUtil();

	function fSendEmail()
	{
		stUnitTest::cInfo('测试发送电子邮件：');

		$mail = new PHPMailer();
		$mail->CharSet = 'UTF-8'; //设置邮件的字符编码，这很重要，不然中文乱码
		//$mail->SMTPDebug = 3;                               // Enable verbose debug output

		$mail->isSMTP();                                      // Set mailer to use SMTP
		$mail->Host = 'smtp.163.com';  // Specify main and backup SMTP servers
		$mail->Port = 25;
		$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
	//	$mail->Port = 587;                                    // TCP port to connect to
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = 'mgtx460@163.com';                 // SMTP username
		$mail->Password = 'yangjie050614';                           // SMTP password

		$mail->setFrom('mgtx460@163.com', 'WseMad');
		$mail->addAddress('826058677@qq.com');     // Add a recipient
	//	$mail->addAddress('176043537@qq.com');
	//	$mail->addAddress('ellen@example.com');               // Name is optional
	//	$mail->addReplyTo('info@example.com', 'Information');
	//	$mail->addCC('cc@example.com');
	//	$mail->addBCC('bcc@example.com');

	//	$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
	//	$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

$l_HtmlBody = <<<HTML
<div>这是用 <b>PHP</b> 发送的电子邮件！</div>
<div><a href="https://wseapp.top">https://wseapp.top?suid=2134567498451313</a></div>
HTML;

		$mail->isHTML(true);                                  // Set email format to HTML
		$mail->Subject = '嗨嗨嗨';
		$mail->Body    = $l_HtmlBody;
	//	$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

		$l_SendRst = $mail->send();
		stUnitTest::cAst($l_SendRst, '发送邮件，结果：' . $mail->ErrorInfo);
	}
//	fSendEmail();


});
//////////////////////////////////// OVER ////////////////////////////////////