<?php
/*
*
*
*/

namespace hpnWse\nSpu\nCaptcha {


/// 文字图像
class stTextImg extends \hpnWse\stSpu
{
	private static $e_ChaSet = 'abcdefghkmnprstuvwxyzABCDEFGHKMNPRSTUVWXYZ';//随机因子 23456789
	private static $e_MiscSet = '~!@#$%^&*';
	private static $e_Code;//验证码
	private static $e_CodeLen = 4;//验证码长度
	private static $e_ImgWid = 120;//宽度
	private static $e_ImgHgt = 30;//高度
	private static $e_Img;//图形资源句柄
	private static $e_FontFam = null;//指定的字体
	private static $e_FontSize = 16;//指定字体大小
	private static $e_FontCloRge = 160;//字体颜色范围
	private static $e_FontAngRge = 20; // 字体角度范围
	private static $e_MiscCloRge = 210;// 杂物颜色范围

	//生成随机码
	private static function eCrtCode()
	{
		$l_End = strlen(self::$e_ChaSet) - 1;
		for ($i=0; $i<self::$e_CodeLen; ++$i)
		{
			self::$e_Code .= self::$e_ChaSet[mt_rand(0, $l_End)];
		}
	}

	//生成背景
	private static function eCrtBkgd()
	{
		self::$e_Img = imagecreatetruecolor(self::$e_ImgWid, self::$e_ImgHgt);
		$l_CloRge = self::$e_FontCloRge + 1;
		$l_Clo = imagecolorallocate(self::$e_Img, mt_rand($l_CloRge ,255), mt_rand($l_CloRge ,255), mt_rand($l_CloRge ,255));
		imagefilledrectangle(self::$e_Img, 0, self::$e_ImgHgt, self::$e_ImgWid, 0, $l_Clo);
	}

	//生成文字
	private static function eCrtText()
	{
		$l_CW = self::$e_ImgWid / self::$e_CodeLen;
		$l_CloRge = self::$e_FontCloRge;
		$l_AngRge = self::$e_FontAngRge;
		$l_BslnY = self::$e_ImgHgt * 2.0 / 3.0;
		for ($i=0; $i<self::$e_CodeLen; ++$i)
		{
			$l_Clo = imagecolorallocate(self::$e_Img, mt_rand(0, $l_CloRge), mt_rand(0, $l_CloRge), mt_rand(0, $l_CloRge));
			imagettftext(self::$e_Img, 
				self::$e_FontSize, 
				mt_rand(-$l_AngRge, +$l_AngRge), 
				$l_CW * $i + mt_rand(4, 8),
				$l_BslnY, 
				$l_Clo, 
				self::$e_FontFam, 
				self::$e_Code[$i]);
		}
	}

	// 生成线条、雪花
	private static function eCrtMisc()
	{
		//线条
		$l_CloRge = self::$e_MiscCloRge;
		$l_Tot = 4;//floor(self::$e_ImgWid / 20);
		for ($i=0; $i<$l_Tot; ++$i)
		{
			$l_Clo = imagecolorallocate(self::$e_Img, mt_rand(0, $l_CloRge), mt_rand(0, $l_CloRge), mt_rand(0, $l_CloRge));
			imageline(self::$e_Img, mt_rand(0, self::$e_ImgWid), mt_rand(0, self::$e_ImgHgt), mt_rand(0, self::$e_ImgWid), mt_rand(0, self::$e_ImgHgt), $l_Clo);
		}

		//雪花
		$l_CloRge = self::$e_MiscCloRge;
		$l_MiscEnd = strlen(self::$e_MiscSet) - 1;
		$l_Tot = 16; //floor(self::$e_ImgWid / 4);
		for ($i=0; $i<$l_Tot; ++$i)
		{
			$l_Clo = imagecolorallocate(self::$e_Img,mt_rand($l_CloRge, 255), mt_rand($l_CloRge, 255), mt_rand($l_CloRge, 255));
			imagestring(self::$e_Img, mt_rand(1, 5), mt_rand(0, self::$e_ImgWid), mt_rand(0, self::$e_ImgHgt), 
				self::$e_MiscSet[mt_rand(0, $l_MiscEnd)], $l_Clo);
		}
	}

	//输出
	private static function eOpt()
	{
		header('Content-Type: image/png');
		\hpnWse\stHttpSvc::cRspsHead_NoCache(); // 禁止缓存

		ob_clean(); // 【必须加上这句！因为缓冲中可能已经内容，会破坏文件格式！】
		imagepng(self::$e_Img);
		imagedestroy(self::$e_Img);
	}

	/// 运行
	///【注意】图像直接输出
	/// a_Prms:
	/// {
	/// c_Wid, c_Hgt: Number，宽高，默认120×30
	/// c_FontSize: Number，字体大小，默认16
	/// }
	/// 返回：
	/// {
	/// c_Code：String，小写验证码
	/// }
	public static function cRun($a_Prms)
	{
		self::$e_ImgWid = \hpnWse\stObjUtil::cFchPpty($a_Prms, 'c_Wid', 120);
		self::$e_ImgHgt = \hpnWse\stObjUtil::cFchPpty($a_Prms, 'c_Hgt', 30);
		self::$e_FontSize = \hpnWse\stObjUtil::cFchPpty($a_Prms, 'c_FontSize', 16);
		self::$e_FontFam = dirname(__FILE__) . '/Font/Elephant.ttf';

		self::eCrtCode();
		self::eCrtBkgd();
		self::eCrtMisc();
		self::eCrtText();
		self::eOpt();
		return array('c_Code' => strtolower(self::$e_Code));
	}
}

} // namespace hpnWse\nSpu\nCaptcha

//////////////////////////////////// OVER ////////////////////////////////////