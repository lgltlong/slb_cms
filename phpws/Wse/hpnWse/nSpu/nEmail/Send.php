
<?php
/*
*
*
*/

namespace hpnWse\nSpu\nEmail {

require_once(__DIR__ . '/PHPMailer/class.smtp.php');
require_once(__DIR__ . '/PHPMailer/class.phpmailer.php');

use \hpnWse\stObjUtil;

/// 发送
class stSend extends \hpnWse\stSpu
{
	/// 运行
	/// $a_Prms
	/// {
	/// c_SmtpSvr: String，SMTP服务器域名，默认'smtp.163.com'
	/// c_Usnm，c_Pswd: String，用户名和口令，必须有效
	/// c_From，c_FromName：String，默认c_Usnm
	/// c_To: String，发送到，必须有效
	/// c_Sbjc：String，标题，必须有效
	/// c_Body: String，邮件体Html
	/// c_fBefSend: void f(PHPMailer a_Mail)
	/// }
	/// 返回：
	/// {
	/// c_IsSucs：Boolean，成功？
	/// c_Exc：Exception，异常，仅当失败时有效
	/// }
	public static function cRun($a_Prms)
	{
		$l_Mail = new \PHPMailer(true); 
		$l_Mail->IsSMTP(); 
		$l_Mail->CharSet='UTF-8'; //设置邮件的字符编码，这很重要，不然中文乱码 
		$l_Mail->SMTPAuth = true; //开启认证 
		$l_Mail->Port = 25; 
		$l_Mail->Host = stObjUtil::cFchPpty($a_Prms, 'c_SmtpSvr', 'smtp.163.com'); 

		$l_Mail->Username = $a_Prms['c_Usnm']; 
		$l_Mail->Password = $a_Prms['c_Pswd'];

	//	$l_Mail->AddReplyTo('网址', '名称'); //回复地址 

		$l_Mail->From = stObjUtil::cFchPpty($a_Prms, 'c_From', $l_Mail->Username); 
		$l_Mail->FromName = stObjUtil::cFchPpty($a_Prms, 'c_FromName', $l_Mail->Username); 
		$to = $a_Prms['c_To'];
		$l_Mail->AddAddress($to); 

		$l_Mail->IsHTML(true); 
		$l_Mail->Subject = $a_Prms['c_Sbjc'];
		$l_Mail->Body = $a_Prms['c_Body'];

		//$l_Mail->WordWrap = 80; // 设置每行字符串的长度 
		//$l_Mail->AddAttachment("f:/test.png"); //可以添加附件 
		
		if (isset($a_Prms['c_fBefSend']))
		{
			call_user_func($a_Prms['c_fBefSend'], $l_Mail);
		}

		try
		{
			$l_Mail->Send();
			return array('c_IsSucs' => true);
		}
		catch (\Exception $a_Exc)
		{
			return array('c_IsSucs' => false, 'c_Exc' => $a_Exc);
		}
	}
}

} // namespace hpnWse\nSpu\nEmail

//////////////////////////////////// OVER ////////////////////////////////////