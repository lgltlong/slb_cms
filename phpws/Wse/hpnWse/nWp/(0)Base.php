<?php
/* WordPress
*
*
*/

namespace hpnWse\nWp {

use \hpnWse\stStrUtil;
use \hpnWse\stObjUtil;
use \hpnWse\stAryUtil;

/// 代码库
class stBase
{
	/// 相对于Web根URL
	public static $c_AppCssDiryUrl = 'css/';
	public static $c_AppJsDiryUrl = 'js/';
	public static $c_AppImgDiryUrl = 'img/';
	public static $c_ArtcDftThmnlUrl = 'ArtcDftThmnl.png'; // 文章默认URL

	/// 获取站点URL
	public static function cGetSiteUrl()
	{
		return site_url() . '/';
	}

	/// 获取目录URL的基
	public static function cGetCtgrUrlBase()
	{
		return \hpnWse\fV1OrV2(get_option( 'category_base' ), 'category');
	}

	/// 转成绝对URL
	public static function cToAbsUrl($a_RelUrl)
	{
		return self::cGetSiteUrl() . $a_RelUrl;
	}

	/// 转成目录URL
	public static function cToCtgrUrl($a_Slug)
	{
	//	return self::cGetSiteUrl() . self::cGetCtgrUrlBase() . '/' . $a_Slug; //【不要用这个，以防伪静态失败】

		$l_Ctgr = get_category_by_slug($a_Slug);
		return $l_Ctgr ? get_category_link($l_Ctgr) : '';
	}

	/// 转成页面URL
	public static function cToPageUrl($a_Tit)
	{
	//	return self::cGetSiteUrl() . $a_RelUrl; //【不要用这个，以防伪静态失败】
		
		$l_Page = get_page_by_title($a_Tit);
		return $l_Page ? $l_Page->guid : '';
	}

	/// 转成图像URL
	public static function cToImgUrl($a_RelUrl)
	{
		return self::cGetSiteUrl() . self::$c_AppImgDiryUrl . $a_RelUrl;
	}

	/// 制作API URL
	public static function cMakeApiUrl($a_C, $a_A)
	{
		if (!$a_C) { $a_C = 'index'; }
		return \hpnWse\fApdUrlQry(self::cToPageUrl('api'), ('_c=' . $a_C . '&_a=' . $a_A)); 
	}

	/// 获取模板目录URL
	public static function cGetTmpDiryUrl()
	{
		return get_template_directory_uri() . '/';
	}

	/// 计算相邻页URL
	/// a_QryPn：String，查询参数里表示分页的参数名，默认“page”
	///		如果查询参数里没有该参数，默认参数值为1
	/// 返回：String[]，null表示不存在前一页
	public static function cCalcAjctPageUrl($a_QryPn = 'page')
	{
		$l_FullUrl = \hpnWse\fGetRqstFullUrl();
		$l_UrlParts = parse_url($l_FullUrl);
		$l_QryPrms = \hpnWse\fPseQryStr($l_UrlParts['query']);
		$l_QryPrms[$a_QryPn] = (isset($l_QryPrms[$a_QryPn])
			? \hpnWse\fV1OrV2(intval($l_QryPrms[$a_QryPn]), 1) : 1);

		$l_Rst = array($l_QryPrms[$a_QryPn] - 1, $l_QryPrms[$a_QryPn] + 1);
		if ($l_Rst[0] <= 0) { $l_Rst[0] = null; }
		if ($l_Rst[1] <= 0) { $l_Rst[1] = null; }
		
		if ($l_Rst[0])
		{
			$l_QryPrms[$a_QryPn] = $l_Rst[0];
			$l_Rst[0] = \hpnWse\fUrlEcdQry($l_UrlParts['path'], $l_QryPrms);
			$l_Rst[0] = \hpnWse\fMakeHttpUrl(null, null, $l_Rst[0]);
		}
		if ($l_Rst[1])
		{
			$l_QryPrms[$a_QryPn] = $l_Rst[1];
			$l_Rst[1] = \hpnWse\fUrlEcdQry($l_UrlParts['path'], $l_QryPrms);
			$l_Rst[1] = \hpnWse\fMakeHttpUrl(null, null, $l_Rst[1]);
		}
		return $l_Rst;
	}

	/// 获取查询分类，仅当URL为“.../category/...”时有效
	public static function cGetQryCtgr($a_Vn = 'cat')
	{
		$l_CatId = get_query_var($a_Vn);
		if (('cat' !== $a_Vn) && is_string($l_CatId)) // 对于自定义分类，若是String，可能是slug
		{
			$l_Taxo = ('cat' == $a_Vn) ? 'category' : $a_Vn;
			$l_Term = self::cGetCtgrBySlug($l_CatId, $l_Taxo);
			return $l_Term;
		}
		return $l_CatId ? get_term($l_CatId) : null;
	}

	/// 根据slug获取目录
	public static function cGetCtgrBySlug($a_Slug, $a_Taxo = 'category')
	{
		return get_term_by('slug', $a_Slug, $a_Taxo);
	}

	/// 获取查询标签，仅当URL为“.../tag/...”时有效
	public static function cGetQryTag()
	{
		$l_TagName = get_query_var('tag');
		if ($l_TagName) { $l_TagName = urldecode($l_TagName); }
		else { return null; }

		$l_Tags = get_tags();
		foreach($l_Tags as $l_Key => $l_Tag)
		{
			if (($l_Tag->slug == $l_TagName) || ($l_Tag->name == $l_TagName))
			{ return $l_Tag; }
		}
		return null;
	}

	/// 获取查询页面名，仅当URL为“.../page/...”，或在页面模板里调用时有效
	public static function cGetQryPageName($a_Vn = 'cat')
	{
		$l_PageName = get_query_var('pagename');
		return $l_PageName ? urldecode($l_PageName) : null;
	}

	/// 获取有序目录，默认子目录在前，父目录在后
	/// a_Ref: WP_Term[]或String，String指定查询目录变量名，默认null表示查询目录'cat'
	public static function cGetOdrdCtgrs($a_Ref = null, $a_Rvs = false)
	{
		global $post;

		$l_NeedSort = true;
		if (is_array($a_Ref)) // 用指定了的
		{
			$l_Ctgrs = $a_Ref;
		}
		else // 先试着用查询目录，无效就用文章
		{
			$l_Taxo = 'category';
			if (is_string($a_Ref))
			{ $l_Taxo = $a_Ref; }

			$l_Ctgr = self::cGetQryCtgr(('category' == $l_Taxo) ? 'cat' : $l_Taxo);
			if ($l_Ctgr)
			{
				$l_Ctgrs = array($l_Ctgr);
				while (0 != $l_Ctgr->parent)
				{
					$l_Ctgr = get_term($l_Ctgr->parent, $l_Taxo);
					$l_Ctgrs[] = $l_Ctgr;
				}
				$l_NeedSort = false;
			}
			else
			{
				$l_Ctgrs = get_the_terms($post, $l_Taxo);//get_the_category();
			}
		}

		if (!is_array($l_Ctgrs) || (count($l_Ctgrs) < 2))
		{ return \hpnWse\fV1OrV2($l_Ctgrs, null); }

		if ($l_NeedSort)
		{
			$l_Copy = $l_Ctgrs; // 做个副本
			\hpnWse\stAryUtil::cSort($l_Ctgrs,
				function ($a_1, $a_2) use($l_Copy)
				{
					if (stBase::cIsAcstCtgr($l_Copy, $a_1, $a_2)) { return +1; }
					if (stBase::cIsAcstCtgr($l_Copy, $a_2, $a_1)) { return -1; }
					return $a_1->term_id - $a_2->term_id; // 并列关系，按ID排
				});
		}
		return $a_Rvs ? array_reverse($l_Ctgrs) : $l_Ctgrs;
	}

	/// 是否为祖先目录
	public static function cIsAcstCtgr(&$a_Ctgrs, $a_A, $a_D)
	{
		if (0 == $a_D->parent) { return false; }
		if ($a_A->term_id == $a_D->term_id) { return false; }
		$l_Lmt = count($a_Ctgrs); // 迭代上限
		$l_P = $a_D;
		while ($l_Lmt > 0)
		{
			$l_PrnIdx = self::eFindPrnCtgr($a_Ctgrs, $l_P);
			if ($l_PrnIdx < 0) { return false; }
			if ($a_Ctgrs[$l_PrnIdx]->term_id == $a_A->term_id) { return true; }
			$l_P = $a_Ctgrs[$l_PrnIdx];
			--$l_Lmt;
		}
		return false;
	}

	public static function eFindPrnCtgr(&$a_Ctgrs, $a_C)
	{
		$l_PrnId = $a_C->parent;
		$l_Idx = \hpnWse\stAryUtil::cFind($a_Ctgrs,
			function ($a_Ary, $a_Idx, $a_Ctgr) use($l_PrnId)
			{
				return $a_Ctgr->term_id == $l_PrnId;
			});
		return $l_Idx;
	}

	/// 获取当前文章所属顶级目录
	public static function cGetTopCtgr($a_Ctgrs = null)
	{
		$l_Ctgrs = $a_Ctgrs ? $a_Ctgrs : self::cGetOdrdCtgrs();
		$l_Len = count($l_Ctgrs);
		return ($l_Len > 0) ? $l_Ctgrs[$l_Len - 1] : null;
	}

	/// 获取当前文章所属顶级目录slug
	public static function cGetTopCtgrSlug($a_Ctgrs = null)
	{
		$l_Rst = self::cGetTopCtgr($a_Ctgrs);
		return $l_Rst->slug;
	}

	/// 获取当前文章所属底级目录
	public static function cGetBtmCtgr($a_Ctgrs = null)
	{
		$l_Ctgrs = $a_Ctgrs ? $a_Ctgrs : self::cGetOdrdCtgrs();
		return $l_Ctgrs[0];
	}

	/// 获取当前文章所属底级目录slug
	public static function cGetBtmCtgrSlug($a_Ctgrs = null)
	{
		$l_Rst = self::cGetBtmCtgr($a_Ctgrs);
		return $l_Rst->slug;
	}

	/// 获取父目录
	public static function cGetPrnCtgr($a_Ctgr, $a_Taxo = 'category')
	{
		return get_term($a_Ctgr->term_id, $a_Taxo);
	}

	/// 获取子目录
	/// $a_Ctgr: WP_Term，目录对象，必须有效
	/// 返回：WP_Term[]
	public static function cGetSubCtgrs($a_Ctgr, $a_Taxo = 'category')
	{
		$l_Rst = array();
		$l_Chds = get_term_children($a_Ctgr->term_id, $a_Taxo); // 返回所有后代的term_id
		foreach ($l_Chds as $l_Idx => $l_Id)
		{
			$l_Dsdt = get_term($l_Id, $a_Taxo);
			if ($a_Ctgr->term_id === $l_Dsdt->parent)
			{ $l_Rst[] = $l_Dsdt; }
		}
		return $l_Rst;
	}

	/// 获取兄弟目录
	public static function cGetSblCtgrs($a_Ctgr, $a_Taxo = 'category')
	{
		$l_PrnCtgr = self::cGetPrnCtgr($a_Ctgr, $a_Taxo);
		$l_Sbls = self::cGetSubCtgrs($l_PrnCtgr, $a_Taxo);
		stAryUtil::cErsIfExi($l_Sbls, null,
			function ($a_Sbls, $a_Idx, $a_Sbl) use($a_Ctgr)
			{
				return $a_Sbl->term_id == $a_Ctgr->term_id;
			});
		return $l_Sbls;
	}


	/// DB - 保存文章元数据
	/// a_Ref：Object，参考对象，键作为要保存的键，值作为默认值
	/// a_Sbmt：Object，提交数据，每个字段若与a_Ref的对应字段类型不同则被转型
	public static function cSaveArtcMeta($a_PostId, &$a_Ref, &$a_Sbmt)
	{
	//	$l_Agms = func_get_args(); $l_AgmAmt = func_num_args();

		foreach ($a_Ref as $l_Key => $l_Dft)
		{
			if (isset($a_Sbmt[$l_Key]))
			{
				$l_Val = $a_Sbmt[$l_Key];
				if (is_int($l_Dft)) { $l_Val = intval($l_Val); }
				else
				if (is_float($l_Dft)) { $l_Val = floatval($l_Val); }
			//	else // 默认就是字符串
			//	{ $l_Val = strval($l_Val); }
			}
			else
			{
				$l_Val = $l_Dft;
			}			
			update_post_meta($a_PostId, $l_Key, \hpnWse\fV1OrV2($l_Val, $l_Dft));
		}
	}

	/// DB - 加载文章元数据
	/// a_Ref：Object，参考对象，键就是要加载的键，值作为默认值
	public static function cLoadArtcMeta($a_PostId, &$a_Ref)
	{
		$l_Rst = array();
		foreach ($a_Ref as $l_Key => $l_Dft)
		{
			$l_Rst[$l_Key] = self::cGetArtcMeta($a_PostId, $l_Key, $l_Dft);
		}
		return $l_Rst;
	}

	/// 获取文章元数据
	public static function cGetArtcMeta($a_PostId, $a_Key, $a_Dft = '')
	{
		$l_Val = get_post_meta($a_PostId, $a_Key, true); // false$String
		if (!\hpnWse\fBool($l_Val)) { return $a_Dft; }

		if (is_int($a_Dft)) { return intval($l_Val); }
		if (is_float($a_Dft)) { return floatval($l_Val); }
		return $l_Val;
	}

	/// 根据ID获取文章
	/// a_PostId：Number，文章ID
	/// a_Prms: 见cQryArtcs
	/// 返回：不存在时为null，否则为文章Array
	public static function cGetArtcById($a_PostId, $a_Prms)
	{
		$l_Artc = get_post($a_PostId);
		return $l_Artc ? self::eFchArtc($l_Artc, $a_Prms) : null;
	}

	/// 查询文章
	/// a_Qry: WP_Query
	/// a_Prms:
	/// {
	/// c_Slug: String，目录标识，必须有效，可以“,”或“+”分隔多个，分别表示“或”“与”
	/// c_Type: String，类型，默认'post'
	/// c_Ofst: Number，文章偏移量，默认0
	/// c_Cpct: Number，容量，默认10
	/// c_ActuOfst：Number，实际偏移量，相对于c_Ofst，默认0，用于计算总数
	/// c_ActuCpct: Number，实际容量，默认PHP_INT_MAX，用于计算总数
	/// c_RmvCtnt：Boolean，不要内容？
	/// c_RmvExcp：Boolean，不要摘要？
	///【忽略】 c_NeedShtTit: Boolean，是否需要短标题
	///【忽略】 c_ShtTitMaxLen: Number，短标题最大长度，默认10
	/// c_NeedAuthor：Boolean，是否需要作者？
	/// c_NeedReadCount: Boolean，是否需要阅读计数？【注意：需要插件“PVC”】
	/// c_NeedFigure：Boolean，是否需要特色图？
	/// c_NeedFigure2：Boolean，是否需要特色图2？【注意：需要插件“MultiPostThumbnails”】
	/// c_NeedTeaser: Boolean，是否需要引导段落？
	/// c_TeaserLen: Number，引导段落长度，当需要从内容提取时使用，默认300
	/// c_TeaserText: Boolean，引导段落文本？若为true则剪掉所有Html标记
	/// c_NeedKeywords: Boolean，需要关键字？
	/// c_NeedMeta: Boolean，需要元信息？
	/// c_MetaKeys: String，列出要获取的元数据键，默认null表示不获取，
	/// 	可以带有类型，如"int Age"，"float Price"，"html Age" ……
	///		【注意：类型后只能跟有一个空格符！】
	/// }
	public static function cQryArtcs($a_Qry, $a_Prms)
	{
		global $post;

		$l_ActuOfst = stObjUtil::cFchPpty($a_Prms, 'c_ActuOfst', 0);
		$l_ActuCpct = stObjUtil::cFchPpty($a_Prms, 'c_ActuCpct', PHP_INT_MAX);

		$l_Rst = array();
		$i = -1;
		while ($a_Qry->have_posts())
		{
			$a_Qry->the_post(); // 写入全局$post
			++$i;
			if (($l_ActuOfst <= $i) && ($i < $l_ActuOfst + $l_ActuCpct))
			{ $l_Rst[] = self::eFchArtc(null, $a_Prms); }
			else
			{ $l_Rst[] = null; }
		}
		wp_reset_postdata();
		return $l_Rst;
	}

	/// 根据分类获取文章，
	///【注意】支持多分类，但若需要文章总数建议使用cFchArtcsAndTotOfCtgrs
	/// a_Prms: 见cQryArtcs
	public static function cGetArtcsOfCtgr($a_Prms)
	{
		$l_Qry = new \WP_Query(array(
			'category_name'=> $a_Prms['c_Slug'], 
			'post_type' => stObjUtil::cFchPpty($a_Prms, 'c_Type', 'post'),
			'posts_per_page'=> stObjUtil::cFchPpty($a_Prms, 'c_Cpct', 10), 
			'offset'=> stObjUtil::cFchPpty($a_Prms, 'c_Ofst', 0))
		);
		return self::cQryArtcs($l_Qry, $a_Prms);
	}

	/// 根据多个分类获取文章及总数
	/// a_Prms: 见cQryArtcs
	/// 返回：void
	public static function cFchArtcsAndTotOfCtgrs(&$a_Artcs, &$a_Tot, $a_Prms)
	{
		$l_Qry = new \WP_Query(array(
			'category_name'=> $a_Prms['c_Slug'], 
			'post_type' => stObjUtil::cFchPpty($a_Prms, 'c_Type', 'post'),
			'posts_per_page'=> PHP_INT_MAX
		));

		// 查询前，确保这两项没有设置
		unset($a_Prms['c_ActuOfst']);
		unset($a_Prms['c_ActuCpct']);
		$a_Artcs = self::cQryArtcs($l_Qry, $a_Prms);
		$a_Tot = $a_Artcs ? count($a_Artcs) : 0;

		if ($a_Tot > 0) // 提取指定范围
		{
			$l_Ofst = stObjUtil::cFchPpty($a_Prms, 'c_Ofst', 0);
			$l_Stop = $l_Ofst + stObjUtil::cFchPpty($a_Prms, 'c_Cpct', 10);
			$a_Artcs = stAryUtil::cSub($a_Artcs, $l_Ofst, $l_Stop);
		}
		else
		{
			$a_Artcs = array();
		}
	}

	/// 获取上一篇文章
	/// $a_Crnt：ID或WP_POST
	/// $a_OfSameCtgr：Boolean，同一目录？
	/// 返回：没有时返回null
	public static function cGetPrevArtc($a_Crnt, $a_OfSameCtgr = false)
	{
		global $post;

		$l_Rst = null;
		$post = get_post($a_Crnt);
		if ($post)
		{
			$l_Rst = get_previous_post($a_OfSameCtgr);
		}
	//	wp_reset_postdata(); //【交给外界】
		return $l_Rst;
	}

	/// 获取下一篇文章
	/// $a_Crnt：ID或WP_POST
	/// $a_OfSameCtgr：Boolean，同一目录？
	/// 返回：没有时返回null
	public static function cGetNextArtc($a_Crnt, $a_OfSameCtgr = false)
	{
		global $post;

		$l_Rst = null;
		$post = get_post($a_Crnt);
		if ($post)
		{
			$l_Rst = get_next_post($a_OfSameCtgr);
		}
	//	wp_reset_postdata(); //【交给外界】
		return $l_Rst;
	}

	/// 获取阅读次数最多的文章，
	///【注意】需要pvc_get_most_viewed_posts，若无效则同cRandArtcsOfCtgr
	/// a_Prms: 见cQryArtcs，但不支持c_Ofst，c_ActuOfst，c_ActuCpct；
	/// 补充：
	/// c_FillWithRand：Boolean，当不足c_Cpct个时，用cRandArtcsOfCtgr填补
	public static function cGetMostReadCntArtcs($a_Prms)
	{
		global $post;

		if (function_exists('pvc_get_most_viewed_posts'))
		{
			$l_Agms = array(); // $a_Prms
			$l_Agms['category_name'] = $a_Prms['c_Slug'];
			$l_Agms['post_type'] = stObjUtil::cFchPpty($a_Prms, 'c_Type', 'post');
			$l_Agms['posts_per_page'] = $l_Cpct = stObjUtil::cFchPpty($a_Prms, 'c_Cpct', 10);
			$l_Rst = pvc_get_most_viewed_posts($l_Agms);
			foreach ($l_Rst as $l_Idx => $l_Artc)
			{
			//	$post = $l_Artc; // 不用了
				$l_Rst[$l_Idx] = self::eFchArtc($l_Artc, $a_Prms);
			}

			if ((count($l_Rst) < $l_Cpct) &&
				stObjUtil::cFchPpty($a_Prms, 'c_FillWithRand'))
			{
				$a_Prms['c_Cpct'] = $l_Cpct - count($l_Rst);
				$l_RandRst = self::cRandArtcsOfCtgr($a_Prms);
				$l_Rst = stAryUtil::cCcat($l_Rst, $l_RandRst);
			}
			return $l_Rst;
		}
		return self::cRandArtcsOfCtgr($a_Prms);
	}

	// 根据分类随机抽取文章
	/// a_Prms: 见cQryArtcs，但不支持c_Ofst，c_ActuOfst，c_ActuCpct
	public static function cRandArtcsOfCtgr($a_Prms)
	{
		$l_Qry = new \WP_Query(array(
			'category_name'=> $a_Prms['c_Slug'], 
			'post_type' => stObjUtil::cFchPpty($a_Prms, 'c_Type', 'post'),
			'posts_per_page'=> stObjUtil::cFchPpty($a_Prms, 'c_Cpct', 10), 
			'orderby'=> 'rand')
		);
		return self::cQryArtcs($l_Qry, $a_Prms);
	}

	/// 获取一篇文章，【注意】必须恰当设置全局$post
	/// a_WpArtc：WP_POST，文章，默认全局$post
	/// a_Prms：见cQryArtcs
	public static function cFchArtc($a_WpArtc, $a_Prms)
	{
		return self::eFchArtc($a_WpArtc, $a_Prms);
	}

	// 获取文章，【警告】必须在循环里使用！
	private static function eFchArtc($a_WpArtc, $a_Prms)
	{
		// 现在数据已在该全局变量里
		global $post;
		global $authordata;

		if (!$a_WpArtc) { $a_WpArtc = &$post; }

		$l_Rst = (array)$a_WpArtc;
		$l_Rst['guid'] = get_the_permalink($a_WpArtc); //【注意：必须在这里修复链接！】
		if (stObjUtil::cFchPpty($a_Prms, 'c_RmvCtnt'))
		{
			$l_Rst['post_content'] = '';
		}
		else
		{
			// get_the_content();【不要调用这个】
			$l_Rst['post_content'] = apply_filters('the_content', $l_Rst['post_content']);		
		}
		if (stObjUtil::cFchPpty($a_Prms, 'c_RmvExcp'))
		{
			$l_Rst['post_excerpt'] = '';
		}
		else
		{
			$l_Rst['post_excerpt'] = get_the_excerpt($a_WpArtc);
			if (!$l_Rst['post_excerpt'] && $l_Rst['post_content'])
			{
				$l_Rst['post_excerpt'] = strip_tags($l_Rst['post_content']);
			}
		}

		// if (stObjUtil::cFchPpty($a_Prms, 'c_NeedShtTit'))
		// {
		// 	$l_Rst['ShtTit'] = $l_Rst['post_title'];
		// 	$l_ShtTitMaxLen = stObjUtil::cFchPpty($a_Prms, 'c_ShtTitMaxLen', 10);
		// 	if (stStrUtil::cGetLen($l_Rst['ShtTit']) > $l_ShtTitMaxLen)
		// 	{
		// 		$l_Rst['ShtTit'] = stStrUtil::cSub($l_Rst['ShtTit'], 0, $l_ShtTitMaxLen - 1);
		// 		$l_Rst['ShtTit'] .= '…';
		// 	}
		// }

		$l_Rst['PostYmd'] = \hpnWse\stDateUtil::cExtrYmd($l_Rst['post_date']);
		$l_Rst['PostMd'] = stStrUtil::cSub($l_Rst['PostYmd'], 5); // 2017-05-24 -> 05-24
		
		if (stObjUtil::cFchPpty($a_Prms, 'c_NeedAuthor'))
		{
		//	$l_Rst['AuthorData'] = (array)$authordata;
			$l_Rst['Author'] = get_the_author();
		}

		if (stObjUtil::cFchPpty($a_Prms, 'c_NeedReadCount') &&
			function_exists('pvc_get_post_views')) // 阅读计数插件
		{ $l_Rst['ReadCount'] = pvc_get_post_views($a_WpArtc->ID); }

		if (stObjUtil::cFchPpty($a_Prms, 'c_NeedFigure'))
		{
			$l_Rst['Figure'] = self::eGetFigurelUrl($l_Rst['ID'], $l_Rst['post_content']);

			if (stObjUtil::cFchPpty($a_Prms, 'c_NeedFigure2'))
			{
				if (class_exists('MultiPostThumbnails'))
				{
					$l_Artc['Figure2'] = MultiPostThumbnails::get_post_thumbnail_url('lxyxk', 'Figure2', $l_Artc['ID']);
				}

				if (!isset($l_Artc['Figure2']))
				{
					$l_Artc['Figure2'] = $l_Artc['Figure'];
				}
			}
		}

		if (stObjUtil::cFchPpty($a_Prms, 'c_NeedTeaser'))
		{
			$l_Ctnt = &$l_Rst['post_content'];
			$l_MoreIdx = stStrUtil::cFind($l_Ctnt, '<!--more-->');
			if ($l_MoreIdx >= 0) // 有“<!--more-->”就用它前面的
			{
				$l_Rst['Teaser'] = stStrUtil::cSub($l_Ctnt, 0, $l_MoreIdx);
				if (stObjUtil::cFchPpty($a_Prms, 'c_TeaserText'))
				{
					$l_Rst['Teaser'] = strip_tags($l_Rst['Teaser']);
				}
			}
			else // 尝试提取
			{
				// 取得文章的纯文本（无Html标记）
				$l_LeadLen = stObjUtil::cFchPpty($a_Prms, 'c_TeaserLen', 300);
				$l_CtntText = strip_tags($l_Ctnt);
				if (stStrUtil::cGetLen($l_CtntText) < $l_LeadLen) // 不长，显示全文
				{
					$l_Rst['Teaser'] = $l_Ctnt;
				}
				else // 截断
				{
					$l_Cut = stStrUtil::cSub($l_CtntText, 0, $l_LeadLen);
					$l_Rst['Teaser'] = $l_Cut;
				}
			}
		}

		if (stObjUtil::cFchPpty($a_Prms, 'c_NeedKeywords'))
		{
			$l_Rst['Keywords'] = \hpnWse\fV1OrV2(
				get_the_terms($l_Rst['ID'], 'post_tag'), array());
			foreach ($l_Rst['Keywords'] as $l_Pn => $l_Pv)
			{
				$l_Link = get_term_link($l_Pv);
				$l_Rst['Keywords'][$l_Pn] = (array)$l_Pv;
				$l_Rst['Keywords'][$l_Pn]['Link'] = $l_Link;
			}
		}

		if (stObjUtil::cFchPpty($a_Prms, 'c_NeedMeta'))
		{
			foreach ($a_Prms['c_MetaKeys'] as $l_Idx => $l_Mk)
			{
				$l_Key = $l_Mk;
				$l_Type = null;
				$l_SpcIdx = stStrUtil::cFind($l_Mk, ' ');
				if ($l_SpcIdx >= 0)
				{
					$l_Type = stStrUtil::cSub($l_Mk, 0, $l_SpcIdx);
					$l_Key = stStrUtil::cSub($l_Mk, $l_SpcIdx + 1);
				}

				$l_Rst[$l_Key] = self::cGetArtcMeta($l_Rst['ID'], $l_Key);
				switch ($l_Type) {
					case 'int':
						{
							$l_Rst[$l_Key] = intval($l_Rst[$l_Key]);
						}
						break;

					case 'float':
						{
							$l_Rst[$l_Key] = floatval($l_Rst[$l_Key]);
						}
						break;

					case 'html':
						{
							$l_Rst[$l_Key] = apply_filters('the_content', $l_Rst[$l_Key]);
						}
						break;
				}
			}
		}
		return $l_Rst;
	}

	/// 获取特色图URL
	private static function eGetFigurelUrl($a_Id, $a_Ctnt)
	{
		global $post, $posts;
		
		$l_1stImg = '';
		$l_Mchs = null;
		 
		// 如果设置了缩略图
		$l_ThmnlId = get_post_thumbnail_id($a_Id);
		if ($l_ThmnlId && \hpnWse\fBool($l_Imgs = wp_get_attachment_image_src($l_ThmnlId, 'full')))
		{
		    $l_1stImg = $l_Imgs[0];
		}
		else // 没有缩略图，查找文章中的第一幅图片
		if (preg_match('/<img[^<>]*?src="([^"]+?)"[^<>]*?>/i', $a_Ctnt, $l_Mchs))
		{
		    $l_1stImg = $l_Mchs[1];
		}
		
		// 既没有缩略图，文中也没有图，设置一幅默认的图片 
		if(!\hpnWse\fBool($l_1stImg))
		{
			$l_1stImg = self::cToImgUrl(self::$c_ArtcDftThmnlUrl);
		}
		return $l_1stImg;
	}

	/// Html - WSE样式表
	public static function cHtml_WseCss($a___ = null)
	{
		$l_Agms = func_get_args(); $l_AgmAmt = func_num_args();
		return self::eHtml_CssJs(true, \hpnWse\fGetWebRootUrl(), 'Wse/cnWse/', $l_Agms, $l_AgmAmt);
	}

	/// Html - WSE脚本
	public static function cHtml_WseJs($a___ = null)
	{
		$l_Agms = func_get_args(); $l_AgmAmt = func_num_args();
		return self::eHtml_CssJs(false, \hpnWse\fGetWebRootUrl(), 'Wse/nWse/', $l_Agms, $l_AgmAmt);
	}

	/// Html - App样式表
	public static function cHtml_AppCss($a___ = null)
	{
		$l_Agms = func_get_args(); $l_AgmAmt = func_num_args();
		return self::eHtml_CssJs(true, self::cGetSiteUrl(), self::$c_AppCssDiryUrl, $l_Agms, $l_AgmAmt);
	}

	/// Html - App脚本
	public static function cHtml_AppJs($a___ = null)
	{
		$l_Agms = func_get_args(); $l_AgmAmt = func_num_args();
		return self::eHtml_CssJs(false, self::cGetSiteUrl(), self::$c_AppJsDiryUrl, $l_Agms, $l_AgmAmt);
	}

	private static function eHtml_CssJs($a_IsCss, $a_RootUrl, $a_DiryUrl, $a_Agms, $a_AgmAmt)
	{
		$l_Rst = '';
		$l_TagBgn = $a_IsCss
			? '<link rel="stylesheet" type="text/css" href="'
			: '<script type="text/javascript" src="';
		$l_TagEnd = $a_IsCss
			? '">'
			: '"></script>';
		for ($i=0; $i<$a_AgmAmt; ++$i)
		{
			if (!\hpnWse\fBool($a_Agms[$i]))
			{ continue; }

			$l_Rst .= $l_TagBgn;
			$l_Rst .= $a_RootUrl;
			$l_Rst .= $a_DiryUrl;
			$l_Rst .= $a_Agms[$i];
			$l_Rst .= $l_TagEnd;
			$l_Rst .= "\r\n";
		}
		return $l_Rst;
	}

	/// Html - 面包屑
	/// a_Eclu：String[]，slug，排除这些目录
	public static function cHtml_Crumbs($a_WrapTagBgn, $a_Bef, $a_Aft, 
							$a_Sprt = null, $a_Taxo = null, $a_Eclu = null)
	{
		if (!\hpnWse\fBool($a_Sprt)) { $a_Sprt = '<span class="cnWse_CrumbsSprt">&gt;</span>'; }
		if (!\hpnWse\fBool($a_Taxo)) { $a_Taxo = 'category'; }

		$l_Rst = \hpnWse\fV1OrV2($a_WrapTagBgn, '');
		if (\hpnWse\fBool($a_Bef)) { $l_Rst .= $a_Bef; $l_Rst .= $a_Sprt; }
		
		$l_Ctgrs = self::cGetOdrdCtgrs($a_Taxo, true); // 反向
		$l_Amt = count($l_Ctgrs);
		for ($i=0; $i<$l_Amt; ++$i)
		{
			if ($a_Eclu && (stAryUtil::cIdxOf($a_Eclu, $l_Ctgrs[$i]->slug) >= 0))
			{ continue; }

			if ($i > 0) { $l_Rst .= $a_Sprt; }
			$l_Rst .= '<a href="';
			$l_Rst .= get_category_link($l_Ctgrs[$i]);// get_term_link($l_Ctgrs[$i], $a_Taxo);
			$l_Rst .= '">';
			$l_Rst .= $l_Ctgrs[$i]->name;
			$l_Rst .= '</a>';
		}

		if (\hpnWse\fBool($a_Aft)) { $l_Rst .= $a_Sprt; $l_Rst .= $a_Aft; }
		if (\hpnWse\fBool($a_WrapTagBgn)) { $l_Rst .= stStrUtil::cPairTag($a_WrapTagBgn); }
		return $l_Rst;
	}

	/// Html - 上一篇（同分类）文章链接
	/// 返回：String，没有时为null
	public static function cHtml_PrevArtcLink($a_Cssc = '', $a_Hash = '', 
		$a_Lab = '上一篇：', $a_NoText = '没有了')
	{
		$l_AjctArtc = get_previous_post(true);
		return self::eBldAjctArtcLink($l_AjctArtc, $a_Cssc, $a_Hash, $a_Lab, $a_NoText);
	}

	/// Html - 下一篇（同分类）文章链接
	/// 返回：String，没有时为null
	public static function cHtml_NextArtcLink($a_Cssc = '', $a_Hash = '', 
		$a_Lab = '下一篇：', $a_NoText = '没有了')
	{
		$l_AjctArtc = get_next_post(true);
		return self::eBldAjctArtcLink($l_AjctArtc, $a_Cssc, $a_Hash, $a_Lab, $a_NoText);
	}

	private static function eBldAjctArtcLink($a_Artc, $a_Cssc, $a_Hash, $a_Lab, $a_NoText)
	{
		if ($a_Artc)
		{
			$l_Rst = 
				'<a class="' . $a_Cssc . '" title="' . $a_Artc->post_title . 
				'" href="' . get_permalink($a_Artc) . $a_Hash .
				'">' . $a_Lab . $a_Artc->post_title . '</a>'
			;
		}
		else
		{
			$l_Rst = 
				'<a class="' . $a_Cssc . '">' . $a_Lab . $a_NoText . '</a>'
			;
		}
		return $l_Rst;
	}

	/// Html - 为SEO生成链接
	/// 每一项可以是String，也可以是Object{ href: "?", title: "?" }
	public static $c_SeoLinks = array();
	public static function cHtml_SeoLinks($a_UrlAry = null)
	{
		$l_Rst = '';
		if (!$a_UrlAry) { $a_UrlAry = self::$c_SeoLinks; }
		foreach ($a_UrlAry as $i => $u)
		{
			if (!$u) { continue; }
			
			$l_Href = $u;
			$l_Title = $u;
			if (is_array($u))
			{
				$l_Href = $u['href'];
				$l_Title = $u['title'];
			}
			$l_Rst .= '<a href="' . $l_Href . '" title="' . $l_Title . '">' . $l_Title . '</a>' . "\r\n";
		}
		return $l_Rst;
	}

	/// Html - 背景图像
	public static function cHtml_BkgdImg($a_Url, $a_IsRel = false)
	{
		if ($a_IsRel)
		{ $a_Url = self::cToImgUrl($a_Url); }
		return 'background-image: url(' . $a_Url . ');';
	}

}


} // namespace hpnWse\nWp

//////////////////////////////////// OVER ////////////////////////////////////