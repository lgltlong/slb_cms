/**
 * Created by eqiao on 2017/7/21.
 */

$(document).ready(function () {

	 /*楼层描点链接*/
	
    var flag = true;
    
    /*头部样式*/
   
    $('.nav ul li a').click(function () {
        $('.nav').addClass('shadow w_navFixed');
        $('.nav ul li a').removeClass('current');
        $(this).addClass('current');
    });
    $('#w_nav1,.goTop').click(function () {
        $('.nav').removeClass('shadow w_navFixed');
        flag = false;
        $("html,body").stop().animate({scrollTop: $("#div1").offset().top-70}, 600, function () {
            flag = true;
        });
    });
    $('#w_nav2').click(function () {
        flag = false;
        $("html,body").stop().animate({scrollTop: $("#div2").offset().top-70}, 600, function () {
            flag = true;
        });
    });
    $('#w_nav3').click(function () {
        flag = false;
        $("html,body").stop().animate({scrollTop: $("#div3").offset().top-70}, 600, function () {
            flag = true;
        });
    });
    $('#w_nav4').click(function () {
        flag = false;
        $("html,body").stop().animate({scrollTop: $("#div4").offset().top-70}, 600, function () {
            flag = true;
        });
    });
    $('#w_nav5').click(function () {
        flag = false;
        $("html,body").stop().animate({scrollTop: $("#div5").offset().top-70}, 600, function () {
            flag = true;
        });
    });
    $('#w_nav6').click(function () {
        flag = false;
        $("html,body").stop().animate({scrollTop: $("#div6").offset().top-70}, 600, function () {
            flag = true;
        });
    });
    $('#w_nav7').click(function () {
        flag = false;
        $("html,body").stop().animate({scrollTop: $("#div7").offset().top - 70}, 600, function () {
            flag = true;
        });
    });
    
	$(window).scroll(function () {
		
		if (!flag) {
            return false
        }
		
        if($(window).scrollTop() - $('#div1').offset().top >= 31){
            $('.nav').addClass('shadow w_navFixed');
        }else{
            $('.nav').removeClass('shadow w_navFixed');
        }
		
        if ($(window).scrollTop() + $(this).height() == $(document).height()) {
        	$('#w_nav7').addClass('current');
            return;
        }
        if ($(window).scrollTop() - $('#div6').offset().top >= -70) {
            $('.nav ul li a').removeClass('current');
        	$('#w_nav6').addClass('current');
            return;
        }
        if ($(window).scrollTop() - $('#div5').offset().top >= -70) {
            $('.nav ul li a').removeClass('current');
        	$('#w_nav5').addClass('current');
            return;
        }
        if ($(window).scrollTop() - $('#div4').offset().top >= -70) {
            $('.nav ul li a').removeClass('current');
        	$('#w_nav4').addClass('current');
            return;
        }
        if ($(window).scrollTop() - $('#div3').offset().top >= -70) {
            $('.nav ul li a').removeClass('current');
        	$('#w_nav3').addClass('current');
            return;
        }
        if ($(window).scrollTop() - $('#div2').offset().top >= -70) {
        	$('.nav ul li a').removeClass('current');
        	$('#w_nav2').addClass('current');
            return;
        }
        if ($(window).scrollTop() - $('#div1').offset().top >= -70) {
        	$('.nav ul li a').removeClass('current');
        	$('#w_nav1').addClass('current');
        }
    });
	
    
//  公司注册

    $('.T_selectHov').hover(function(){
    	$(this).parent().addClass('T_selectLi');
		$(this).find('.T_selectBot').stop().animate({'margin-top':-80},500);
	},function(){
    	$(this).parent().removeClass('T_selectLi');
		$(this).find('.T_selectBot').stop().animate({'margin-top':0},500);
	})
    
    $('.T_register ul li b').hover(function(){
		$(this).parent().find('span').stop().animate({'top':-10},500);
	},function(){
		$(this).parent().find('span').stop().animate({'top':0},500);
	})
    var index = 0;
    $('.T_strategy1 .T_strategyNext').click(function(){
    	$('.T_strategy1 .T_strategyPre').addClass('T_strategyPres');
    	if(index >= $('.T_strategy1 .T_strategyR ul li').length - 1){
    		index = $('.T_strategy1 .T_strategyR ul li').length - 1;
    		$('.T_strategy1 .T_strategyNext').addClass('T_strategyNexts');
    	}else{
    		index++;
    		$('.T_strategy1 .T_strategyNext').removeClass('T_strategyNexts');
    	}
    	$('.T_strategy1 .T_strategyR ul li').eq(index).fadeIn().siblings().fadeOut();
    })
    $('.T_strategy1 .T_strategyPre').click(function(){
    	$('.T_strategy1 .T_strategyNext').removeClass('T_strategyNexts');
    	if(index == 0){
    		index = 0;
    		$('.T_strategy1 .T_strategyPre').removeClass('T_strategyPres');
    	}else{
    		index--;
    		$('.T_strategy1 .T_strategyPre').addClass('T_strategyPres');
    	}
    	$('.T_strategy1 .T_strategyR ul li').eq(index).fadeIn().siblings().fadeOut();
    })
    var index1 = 0;
    $('.T_strategy2 .T_strategyNext').click(function(){
    	$('.T_strategy2 .T_strategyPre').addClass('T_strategyPres');
    	if(index >= $('.T_strategy2 .T_strategyR ul li').length - 1){
    		index = $('.T_strategy2 .T_strategyR ul li').length - 1;
    		$('.T_strategy2 .T_strategyNext').addClass('T_strategyNexts');
    	}else{
    		index++;
    		$('.T_strategy2 .T_strategyNext').removeClass('T_strategyNexts');
    	}
    	$('.T_strategy2 .T_strategyR ul li').eq(index).fadeIn().siblings().fadeOut();
    })
    $('.T_strategy2 .T_strategyPre').click(function(){
    	$('.T_strategy2 .T_strategyNext').removeClass('T_strategyNexts');
    	if(index == 0){
    		index = 0;
    		$('.T_strategy2 .T_strategyPre').removeClass('T_strategyPres');
    	}else{
    		index--;
    		$('.T_strategy2 .T_strategyPre').removeClass('T_strategyPres');
    	}
    	$('.T_strategy2 .T_strategyR ul li').eq(index).fadeIn().siblings().fadeOut();
    })
    var index2 = 0;
    $('.T_strategy3 .T_strategyNext').click(function(){
    	$('.T_strategy3 .T_strategyPre').addClass('T_strategyPres');
    	if(index >= $('.T_strategy3 .T_strategyR ul li').length - 1){
    		index = $('.T_strategy3 .T_strategyR ul li').length - 1;
    		$('.T_strategy3 .T_strategyNext').addClass('T_strategyNexts');
    	}else{
    		index++;
    		$('.T_strategy3 .T_strategyNext').removeClass('T_strategyNexts');
    	}
    	$('.T_strategy3 .T_strategyR ul li').eq(index).fadeIn().siblings().fadeOut();
    })
    $('.T_strategy3 .T_strategyPre').click(function(){
    	$('.T_strategy3 .T_strategyNext').removeClass('T_strategyNexts');
    	if(index == 0){
    		index = 0;
    		$('.T_strategy3 .T_strategyPre').removeClass('T_strategyPres');
    	}else{
    		index--;
    		$('.T_strategy3 .T_strategyPre').addClass('T_strategyPres');
    	}
    	$('.T_strategy3 .T_strategyR ul li').eq(index).fadeIn().siblings().fadeOut();
    })
    $('.T_qualified dl.last dd a').click(function(){
    	$('.T_qualified dl.last').toggleClass('lasts');
    	$(this).hide().siblings().show();
    })
    
    $('.T_services ul li').hover(function(){
		$(this).find('.T_serviceBot').stop().animate({'top':-1},350,function(){
			$(this).siblings().css('border',0);
		});
	},function(){
		$(this).find('.T_serviceTop').css('border','1px solid #fff');
		$(this).find('.T_serviceBot').stop().animate({'top':190},350);
	})
    
    $('.T_service dl dd a').hover(function(){
		$(this).parent().find('em').toggleClass('color');
	})
    
    $(".T_select ul").scrollScreen({callback:function(){
        $(".T_select ul").each(function(){
        	$('.T_select ul li').addClass('animated');
            $('.T_select ul li').eq(0).addClass('fadeInLeft');
            $('.T_select ul li').eq(1).addClass('fadeInDown');
            $('.T_select ul li').eq(2).addClass('fadeInDown');
            $('.T_select ul li').eq(3).addClass('fadeInRight');
            $('.T_select ul li').eq(4).addClass('fadeInUp');
            $('.T_select ul li').eq(5).addClass('fadeInUp');
            $('.T_select ul li').eq(6).addClass('fadeInUp');
            $('.T_select ul li').eq(7).addClass('fadeInUp');
        });
    }})
    $(".T_register ul").scrollScreen({callback:function(){
        $(".T_register ul").each(function(){
        	$('.T_register ul li').addClass('animated bounceInUp');
        });
    }})
    $(".T_qualified1").scrollScreen({callback:function(){
        $(".T_qualified1").each(function(){
        	$('.T_qualified1 img,.T_qualified1 dl').addClass('animated');
            $('.T_qualified1 img').addClass('fadeInLeft');
            $('.T_qualified1 dl').addClass('fadeInRight');
        });
    }})
    $(".T_qualified2").scrollScreen({callback:function(){
        $(".T_qualified2").each(function(){
        	$('.T_qualified2 img,.T_qualified2 dl').addClass('animated');
            $('.T_qualified2 dl').addClass('fadeInLeft');
            $('.T_qualified2 img').addClass('fadeInRight');
        });
    }})
    $(".T_service dl dt").scrollScreen({callback:function(){
        $(".T_service dl").each(function(){
        	$('.T_service dl dt,.T_service dl dd').addClass('animated');
            $('.T_service dl dt').eq(0).addClass('fadeInLeft');
            $('.T_service dl dd').eq(0).addClass('fadeInDown');
            $('.T_service dl dd').eq(1).addClass('fadeInDown');
            $('.T_service dl dd').eq(2).addClass('fadeInRight');
            $('.T_service dl dd').eq(3).addClass('fadeInUp');
            $('.T_service dl dd').eq(4).addClass('fadeInUp');
            $('.T_service dl dd').eq(5).addClass('fadeInRight');
        });
    }})
    $(".T_services ul li").scrollScreen({callback:function(){
        $(".T_services ul").each(function(){
        	$('.T_services ul li').addClass('animated zoomIn');
        });
    }})
    
    $('.T_register ul li b.T_commonP1').click(function(){
    	$("html,body").stop().animate({scrollTop: $("#div3").offset().top-70}, 600);
    })
    $('.T_register ul li b.T_commonP2').click(function(){
    	$("html,body").stop().animate({scrollTop: $("#div4").offset().top-70}, 600);
    })
    $('.T_register ul li b.T_commonP3').click(function(){
    	$("html,body").stop().animate({scrollTop: $("#div5").offset().top-70}, 600);
    })
    $('.T_register ul li b.T_commonP4').click(function(){
    	$("html,body").stop().animate({scrollTop: $("#div6").offset().top-70}, 600);
    })
});





