<?php
namespace Home\Controller;
use Think\Controller;
class UserController extends CommonController {
   
	public function _initialize() {
		parent::_initialize();
		$this->dbname = CONTROLLER_NAME;
	}



	public function _befor_add(){
		$list=orgcateTree($pid=0,$level=0,$type=0);
		$this->assign('list',$list);
	}

	public function _befor_insert($data){
		$password=md5(md5(I('pwd')));
		$data['password']=$password;
		unset($data['pwd']);
		return $data;
	}



	public function _befor_edit(){
		$list=orgcateTree($pid=0,$level=0,$type=0);
		$this->assign('list',$list);
	}

	public function _befor_update($data){
		if (strlen(I('pwd'))!==32){
			$password=md5(md5(I('pwd')));
			$data['password']=$password;
		}
		unset($data['pwd']);
		return $data;
	}

	//编辑用户权限   
	public function editrule(){
		$uid = I('get.id');
		$dep = I('get.depname');
		$pos = I('get.posname');

		# 判断是增加权限还是删除权限
		if( M('auth_group_access')->where('uid='.$uid)->count() > 0 ){
			M('auth_group_access')->where('uid='.$uid)->delete();
		}else{
			# 增加用户部门权限
			$group_id =  M('auth_group')->where("title='$dep'")->getField('id');
			M('auth_group_access')->add( array('uid'=>$uid, 'group_id'=>$group_id ) );

			# 增加用户职位权限
			$group_id = M('auth_group')->where("title='$pos'")->getField('id');
			M('auth_group_access')->add( array('uid'=>$uid, 'group_id'=>$group_id ) );
		}

		$this->mtReturn(200,"设置成功".$id,$_REQUEST['navTabId'],false); 
	}

	public function _befor_del($id){
		$uid=$id; 
		M('auth_group_access')->where('uid='.$uid.'')->delete(); 
	}
		
}