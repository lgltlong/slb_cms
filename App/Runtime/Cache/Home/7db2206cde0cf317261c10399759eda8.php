<?php if (!defined('THINK_PATH')) exit();?><div class="bjui-pageContent">
        <div class="pageFormContent" style="margin:1px;" data-layout-h="0">
		          <ul class="nav nav-tabs" role="tablist">
                   <li class="active"><a href="#cust" role="tab" data-toggle="tab">客户管理</a></li>
					<li><a href='#custcon' role='tab' data-toggle='tab'>联系人</a></li><li><a href='#custgd' role='tab' data-toggle='tab'>跟单记录</a></li><li><a href='#hetong' role='tab' data-toggle='tab'>合同管理</a></li>
                  </ul>
		 <div class="tab-content">
           <div class="tab-pane fade active in" id="cust"><table class="table table-bordered table-striped table-hover">
           <tbody>
		   <tr><td><label for='id_input' class='control-label x85'>ID:</label><?php echo ($Rs["id"]); ?></td><td><label for='title_input' class='control-label x85'>公司名称:</label><?php echo ($Rs["title"]); ?></td></tr>
<tr><td><label for='dizhi_input' class='control-label x85'>公司地址:</label><?php echo ($Rs["dizhi"]); ?></td><td><label for='fenlei_input' class='control-label x85'>进展:</label><?php echo ($Rs["fenlei"]); ?></td></tr>
<tr><td><label for='xcrq_input' class='control-label x85'>下次联系:</label><?php echo ($Rs["xcrq"]); ?></td><td><label for='xingming_input' class='control-label x85'>联系人:</label><?php echo ($Rs["xingming"]); ?></td></tr>
<tr><td><label for='sex_input' class='control-label x85'>性别:</label><?php echo ($Rs["sex"]); ?></td><td><label for='bumen_input' class='control-label x85'>部门职务:</label><?php echo ($Rs["bumen"]); ?></td></tr>
<tr><td><label for='phone_input' class='control-label x85'>手机号码:</label><?php echo ($Rs["phone"]); ?></td><td><label for='email_input' class='control-label x85'>EMAIL:</label><?php echo ($Rs["email"]); ?></td></tr>
<tr><td><label for='qq_input' class='control-label x85'>QQ:</label><?php echo ($Rs["qq"]); ?></td><td><label for='name_input' class='control-label x85'>爱好:</label><?php echo ($Rs["name"]); ?></td></tr>
<tr><td><label for='type_input' class='control-label x85'>客户来源:</label><?php echo ($Rs["type"]); ?></td><td><label for='juname_input' class='control-label x85'>分享给:</label><?php echo ($Rs["juname"]); ?></td></tr>
<tr><td><label for='uname_input' class='control-label x85'>添加人:</label><?php echo ($Rs["uname"]); ?></td><td><label for='addtime_input' class='control-label x85'>添加时间:</label><?php echo ($Rs["addtime"]); ?></td></tr>
<tr><td><label for='uuname_input' class='control-label x85'>更新人:</label><?php echo ($Rs["uuname"]); ?></td><td><label for='updatetime_input' class='control-label x85'>更新时间:</label><?php echo ($Rs["updatetime"]); ?></td></tr>
<tr></tr>
<tr><td colspan=2><label for='beizhu_input' class='control-label x85'>客户描述:</label><textarea name='beizhu'  cols='65' rows='5' ><?php echo ($Rs["beizhu"]); ?></textarea></td></tr>
           </tbody>
          </table>
		  </div>
		  
		 <div class='tab-pane fade' id='custcon'><table class='table table-bordered table-striped table-hover'><?php $custconlist=M('custcon')->where(array('jcid'=>I('id')))->select(); ?> <thead> <tr><th>ID</th>
<th height=30>客户名称</th>
<th>姓名</th>
<th>性别</th>
<th height=30>部门职务</th>
<th height=30>手机号码</th>
<th height=30>EMAIL</th>
<th height=30>QQ</th>
<th>添加时间</th>
<th>更新时间</th>
<th>详细</th></tr></thead><tbody><?php if(is_array($custconlist)): foreach($custconlist as $key=>$custconv): ?><tr><td><?php echo ($custconv["id"]); ?></td>
<td><?php echo (msubstr($custconv["jcname"],0,20)); ?></td>
<td><?php echo ($custconv["xingming"]); ?></td>
<td><?php echo ($custconv["sex"]); ?></td>
<td><?php echo (msubstr($custconv["bumen"],0,20)); ?></td>
<td><?php echo (msubstr($custconv["phone"],0,20)); ?></td>
<td><?php echo (msubstr($custconv["email"],0,20)); ?></td>
<td><?php echo (msubstr($custconv["qq"],0,20)); ?></td>
<td><?php echo (substr($custconv["addtime"],0,10)); ?></td>
<td><?php echo (substr($custconv["updatetime"],0,10)); ?></td>
  <td><a href='/index.php/Home/custcon/view/id/<?php echo ($custconv['id']); ?>/navTabId/<?php echo CONTROLLER_NAME;?>'  data-toggle='dialog' data-width='800' data-height='500' data-id='dialog-maskcustcon' data-mask='true' >详细</a></td></tr><?php endforeach; endif; ?> </tbody> </table></div><div class='tab-pane fade' id='custgd'><table class='table table-bordered table-striped table-hover'><?php $custgdlist=M('custgd')->where(array('jcid'=>I('id')))->select(); ?> <thead> <tr><th>ID</th>
<th height=30>详情</th>
<th height=30>客户名称</th>
<th>跟单方式</th>
<th>进展阶段</th>
<th>下次联系</th>
<th height=30>跟单人</th>
<th>跟单时间</th>
<th>更新时间</th>
<th>详细</th></tr></thead><tbody><?php if(is_array($custgdlist)): foreach($custgdlist as $key=>$custgdv): ?><tr><td><?php echo ($custgdv["id"]); ?></td>
<td><?php echo (msubstr($custgdv["value"],0,20)); ?></td>
<td><?php echo (msubstr($custgdv["jcname"],0,20)); ?></td>
<td><?php echo ($custgdv["type"]); ?></td>
<td><?php echo ($custgdv["fenlei"]); ?></td>
<td><?php echo (substr($custgdv["xcrq"],0,10)); ?></td>
<td><?php echo (msubstr($custgdv["uname"],0,20)); ?></td>
<td><?php echo (substr($custgdv["addtime"],0,10)); ?></td>
<td><?php echo (substr($custgdv["updatetime"],0,10)); ?></td>
  <td><a href='/index.php/Home/custgd/view/id/<?php echo ($custgdv['id']); ?>/navTabId/<?php echo CONTROLLER_NAME;?>'  data-toggle='dialog' data-width='800' data-height='500' data-id='dialog-maskcustgd' data-mask='true' >详细</a></td></tr><?php endforeach; endif; ?> </tbody> </table></div><div class='tab-pane fade' id='hetong'><table class='table table-bordered table-striped table-hover'><?php $hetonglist=M('hetong')->where(array('jcid'=>I('id')))->select(); ?> <thead> <tr><th>ID</th>
<th>签约日期</th>
<th height=30>客户名称</th>
<th height=30>联系人</th>
<th height=30>联系电话</th>
<th>合同金额</th>
<th>业务员</th>
<th>添加人</th>
<th>到期日期</th>
<th>更新时间</th>
<th>详细</th></tr></thead><tbody><?php if(is_array($hetonglist)): foreach($hetonglist as $key=>$hetongv): ?><tr><td><?php echo ($hetongv["id"]); ?></td>
<td><?php echo (substr($hetongv["addtime"],0,10)); ?></td>
<td><?php echo (msubstr($hetongv["jcname"],0,20)); ?></td>
<td><?php echo (msubstr($hetongv["xingming"],0,20)); ?></td>
<td><?php echo (msubstr($hetongv["dianhua"],0,20)); ?></td>
<td><?php echo ($hetongv["jine"]); ?></td>
<td><?php echo ($hetongv["name"]); ?></td>
<td><?php echo ($hetongv["uname"]); ?></td>
<td><?php echo (substr($hetongv["dqrq"],0,10)); ?></td>
<td><?php echo (substr($hetongv["updatetime"],0,10)); ?></td>
  <td><a href='/index.php/Home/hetong/view/id/<?php echo ($hetongv['id']); ?>/navTabId/<?php echo CONTROLLER_NAME;?>'  data-toggle='dialog' data-width='800' data-height='500' data-id='dialog-maskhetong' data-mask='true' >详细</a></td></tr><?php endforeach; endif; ?> </tbody> </table></div>
		   
		 </div>
        </div>
		<div class="bjui-footBar">
        <ul>
            <li><button type="button" class="btn-close" data-icon="close">关闭</button></li>
        </ul>
    </div>
</div>