<?php if (!defined('THINK_PATH')) exit();?><div class="bjui-pageHeader">
<form id="pagerForm" data-toggle="ajaxsearch" action="/index.php/Home/Cust" method="post">
	
	<input type="hidden" name="pageSize" value="<?php echo ($numPerPage); ?>">
    <input type="hidden" name="pageCurrent" value="<?php echo ((isset($_REQUEST['pageNum']) && ($_REQUEST['pageNum'] !== ""))?($_REQUEST['pageNum']):1); ?>">
	 
        <div class="bjui-searchBar">
            <label>关键词：</label><input type="text" value="<?php echo ($_REQUEST['keys']); ?>" name="keys" class="form-control" size="15" />
			<label>添加时间：</label><input type="text" data-toggle='datepicker' value="<?php echo ($_REQUEST['time1']); ?>" name="time1" class="form-control" size="15" />-<input type="text" data-toggle='datepicker' value="<?php echo ($_REQUEST['time2']); ?>" name="time2" class="form-control" size="15" />
             <button type="submit"  class="btn-default" data-icon="search">查询</button>
              <a class="btn btn-orange" href="javascript:;" onclick="$(this).navtab('reloadForm', true);" data-icon="undo">清空查询</a>
			  <span <?php echo display(CONTROLLER_NAME.'/del'); ?> style="float:right;" ><a href="/index.php/Home/Cust/del/navTabId/<?php echo CONTROLLER_NAME;?>" class="btn btn-red" data-toggle="doajax" data-confirm-msg="确定要清理吗？" data-icon="remove">清理</a></span> 
			  <span <?php echo display(CONTROLLER_NAME.'/outxls'); ?> style="float:right;margin-right:20px;"><a href="/index.php/Home/Cust/outxls" class="btn btn-blue" data-toggle="doexport" data-confirm-msg="确定要导出吗？" data-icon="arrow-up">导出</a></span>

        <span <?php echo display(CONTROLLER_NAME.'/xlsin'); ?> style="float:right;margin-right:20px;"><a href="/index.php/Home/Cust/xlsinin/navTabId/<?php echo CONTROLLER_NAME;?>" class="btn btn-blue" data-toggle="dialog" data-width="900" data-height="500" data-confirm-msg="确定要导入吗？" data-icon="arrow-up">导入</a></span>

			  <span <?php echo display(CONTROLLER_NAME.'/add'); ?> style="float:right;margin-right:20px;"><a href="/index.php/Home/Cust/add/navTabId/<?php echo CONTROLLER_NAME;?>" class="btn btn-green" data-toggle="dialog" data-width="900" data-height="500" data-id="dialog-mask" data-mask="true" data-icon="plus">新增</a></span>
		</div> 
</form>

<!--分享表单-->
<form id="sharefm" style="margin-top:3px;">
  <label for='juid_input' class='control-label x85' style="width:48px;">分享给:</label><input type='text' id='juid' name='juid'  size='20' data-toggle='lookup' data-url='/index.php/Home/public/user/navTabId/<?php echo CONTROLLER_NAME;?>'  value='<?php echo ($Rs["juid"]); ?>'  >

  <label for='juname_input' class='control-label x85'>分享给:</label><input type='text' id='juname' name='juname'  size='20' data-toggle='lookup' data-url='/index.php/Home/public/user/navTabId/<?php echo CONTROLLER_NAME;?>'  value='<?php echo ($Rs["juname"]); ?>'  >

  <button type="button" class="btn btn-primary" onclick="custShare()">分享</button>
</form>
    
</div>
<div class="bjui-pageContent">
  <form id="custID">
     <table data-toggle="tablefixed" data-width="100%" data-layout-h="0" data-nowrap="true">
        <thead>
            <tr>
              <th width="10" height="30"></th>
              <th data-order-direction='desc' data-order-field='id'>ID</th>
              <th>公司名称</th>
              <th data-order-direction='desc' data-order-field='fenlei'>进展</th>
              <th data-order-direction='desc' data-order-field='xcrq'>下次联系</th>
              <th>联系人</th>
              <th>手机号码</th>
              
              <th>客户来源</th>
              <th data-order-direction='desc' data-order-field='uname'>添加人</th>
              <th data-order-direction='desc' data-order-field='addtime'>添加时间</th>
              <th data-order-direction='desc' data-order-field='updatetime'>更新时间</th>

              <th><button type="button" class="btn btn-link" onclick="selectall()">全选</button></th>
              <th>详细</th>
              <th <?php echo display(CONTROLLER_NAME.'/del'); ?> >状态</th>
              <th <?php echo display(CONTROLLER_NAME.'/edit'); ?> >编辑</th>
            </tr>
        </thead>

        <tbody>
          <?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><tr>
            <td></td>
            <td><?php echo ($v["id"]); ?></td>
            <td><?php echo (msubstr($v["title"],0,20)); ?></td>
            <td><?php echo ($v["fenlei"]); ?></td>
            <td><?php echo (substr($v["xcrq"],0,10)); ?></td>
            <td><?php echo (msubstr($v["xingming"],0,20)); ?></td>
            <td><?php echo (msubstr($v["phone"],0,20)); ?></td>
            
            <td><?php echo (msubstr($v["type"],0,20)); ?></td>
            <td><?php echo ($v["uname"]); ?></td>
            <td><?php echo (substr($v["addtime"],0,10)); ?></td>
            <td><?php echo (substr($v["updatetime"],0,10)); ?></td>
            <td><input type="checkbox" name="slt[]" value="<?php echo ($v["id"]); ?>"></td>

            <td><a href="/index.php/Home/Cust/view/id/<?php echo ($v['id']); ?>/navTabId/<?php echo CONTROLLER_NAME;?>"  data-toggle="dialog" data-width="900" data-height="500" data-id="dialog-mask" data-mask="true" >详细</a></td>

            <td <?php echo display(CONTROLLER_NAME.'/del'); ?> ><a href="/index.php/Home/Cust/del/id/<?php echo ($v['id']); ?>/navTabId/<?php echo CONTROLLER_NAME;?>" data-toggle="doajax" data-confirm-msg="确定要操作吗？"><?php if($v["status"] == 1 ): ?><img src="/Public/images/ok.gif" border="0"/><?php else: ?><img src="/Public/images/locked.gif" border="0"/><?php endif; ?></a></td>

            <td <?php echo display(CONTROLLER_NAME.'/edit'); ?> > 
            <a href="/index.php/Home/custcon/add/cid/<?php echo ($v['id']); ?>/navTabId/<?php echo CONTROLLER_NAME;?>"  class="btn btn-green btn-sm" data-toggle="dialog" data-width="900" data-height="500" data-id="dialog-mask" data-mask="true">联系人</a>
            <a href="/index.php/Home/custgd/add/cid/<?php echo ($v['id']); ?>/navTabId/<?php echo CONTROLLER_NAME;?>"   class="btn btn-green btn-sm" data-toggle="dialog" data-width="900" data-height="500" data-id="dialog-mask" data-mask="true">进展</a>
            <a href="/index.php/Home/Cust/edit/id/<?php echo ($v['id']); ?>/navTabId/<?php echo CONTROLLER_NAME;?>"   class="btn btn-green btn-sm" data-toggle="dialog" data-width="900" data-height="500" data-id="dialog-mask" data-mask="true" >编辑</a>
            </td>
		        </td>
          </tr><?php endforeach; endif; else: echo "" ;endif; ?>
        </tbody>
    </table>
  </form>
  
    <script type="text/javascript">
      isselected = false;

      function selectall(){
        if(isselected){
          $("input[type='checkbox']").each(function(){
            this.checked = false;
          });
          isselected = false;
        }else{
          $("input[type='checkbox']").each(function(){
            this.checked = true;
          });
          isselected = true;
        }

      }

      // 执行分享
      function custShare(){
        $.ajax({
          url: "/index.php/Home/Cust/cust_share/id/999",
          type: 'post',
          dataType: "json",
          data: $("#sharefm").serialize() + "&" + $("#custID").serialize(),
          success: function(res){
            console.log(res);
            alert("分享成功");
          },
          error: function(err){
            console.warn(err);
            if( err.status == 200 ){
              alert("分享成功");
            }else{
              alert("分享失败，请重新提交");
            }
            
          }
        });
      }
    </script>


    <div class="bjui-footBar">
        <div class="pages">
            <span>共 <?php echo ($totalCount); ?> 条  每页 <?php echo ($numPerPage); ?> 条</span>
        </div>
	    <div class="pagination-box" data-toggle="pagination" data-total="<?php echo ($totalCount); ?>" data-page-size="<?php echo ($numPerPage); ?>" data-page-current="<?php echo ($currentPage); ?>">
        </div>
    </div>
</div>