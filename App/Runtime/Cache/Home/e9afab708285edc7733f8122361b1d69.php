<?php if (!defined('THINK_PATH')) exit();?><div class="bjui-pageContent">
    <form action="/index.php/Home/Custgd/add/navTabId/<?php echo CONTROLLER_NAME;?>" class="pageForm" data-toggle="validate">
		<input type="hidden" name="id" value="<?php echo ($id); ?>">
        <div class="pageFormContent" data-layout-h="0">
          <table class="table table-condensed table-hover" width="100%">
           <tbody>
			<tr><td><label for='jcid_input' class='control-label x85'>客户ID:</label><input type='text' id='jcid' name='jcid'  size='20' data-toggle='lookup' data-url='/index.php/Home/public/cname/navTabId/<?php echo CONTROLLER_NAME;?>'  value='<?php echo ($jcid); ?>'  ></td></tr>
<tr><td><label for='jcname_input' class='control-label x85'>客户名称:</label><input type='text' id='jcname' name='jcname'  size='20' data-toggle='lookup' data-url='/index.php/Home/public/cname/navTabId/<?php echo CONTROLLER_NAME;?>'  value='<?php echo ($jcname); ?>'  ></td><td><label for='type_input' class='control-label x85'>跟单方式:</label><select name='type'  data-toggle='selectpicker' ><option value=''>请选择</option><?php if(is_array(C("CUSTGD_TYPE"))): $i = 0; $__LIST__ = C("CUSTGD_TYPE");if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$type): $mod = ($i % 2 );++$i;?><option value='<?php echo ($type); ?>' <?php if( $type == $Rs['type'] ): ?>selected<?php endif; ?> ><?php echo ($type); ?></option><?php endforeach; endif; else: echo "" ;endif; ?></td></tr>
<tr><td><label for='fenlei_input' class='control-label x85'>进展阶段:</label><select name='fenlei'  data-toggle='selectpicker' ><option value=''>请选择</option><?php if(is_array(C("CUSTGD_FENLEI"))): $i = 0; $__LIST__ = C("CUSTGD_FENLEI");if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$fenlei): $mod = ($i % 2 );++$i;?><option value='<?php echo ($fenlei); ?>' <?php if( $fenlei == $Rs['fenlei'] ): ?>selected<?php endif; ?> ><?php echo ($fenlei); ?></option><?php endforeach; endif; else: echo "" ;endif; ?></td><td><label for='xcrq_input' class='control-label x85'>下次联系:</label><input type='text' data-toggle='datepicker' id='xcrq' name='xcrq'   size='20'   value='<?php echo (substr($Rs["xcrq"],0,10)); ?>'  ></td></tr>
<tr><td colspan=2><label for='value_input' class='control-label x85'>详情:</label><textarea name='value'  cols='65' rows='5' ><?php echo ($Rs["value"]); ?></textarea></td></tr>
           </tbody>
          </table>
        </div>
        <div class="bjui-footBar">
            <ul>
                <li><button type="button" class="btn-close" data-icon="close">取消</button></li>
                <li><button type="submit" class="btn-default" data-icon="save">保存</button></li>
            </ul>
        </div>
    </form>
</div>